package root.b;

public class Light {

    private String room;

    public Light(String room) {
        this.room = room;
    }

    public void on() {
        System.out.println("свет включен в " + room);
    }

    public void off() {
        System.out.println("свет выключен в " + room);
    }

}
