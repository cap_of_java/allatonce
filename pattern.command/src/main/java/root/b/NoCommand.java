package root.b;

public class NoCommand implements Command {
    @Override
    public void execute() {
        throw new RuntimeException("NoCommand was called!!!");
    }

    @Override
    public void undo() {
        throw new RuntimeException("Attempt to call undo() on NoCommand");
    }
}
