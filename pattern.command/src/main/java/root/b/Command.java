package root.b;

public interface Command {

    void execute();

    void undo();

}
