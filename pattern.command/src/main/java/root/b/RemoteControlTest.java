package root.b;

public class RemoteControlTest {

    public static void main(String[] args) {
        SimpleRemoteControl remote = new SimpleRemoteControl();
        Light light = new Light("Living room");
        LightOnCommand lightOn = new LightOnCommand(light);

        GarageDoor door = new GarageDoor("");
        GarageDoorUpCommand openGarageDoor = new GarageDoorUpCommand(door);

        remote.setCommand(lightOn);
        remote.buttonWasPressed();
        remote.setCommand(openGarageDoor);
        remote.buttonWasPressed();
    }

}
