package root.b;

public class TVOnCommand implements Command {

    TV tv;

    public TVOnCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.turnTVon();
    }

    @Override
    public void undo() {
        tv.turnTVOff();
    }
}
