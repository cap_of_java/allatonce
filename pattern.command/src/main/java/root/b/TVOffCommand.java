package root.b;

public class TVOffCommand implements Command {

    TV tv;

    public TVOffCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.turnTVOff();
    }

    @Override
    public void undo() {
        tv.turnTVon();
    }
}
