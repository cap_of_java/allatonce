package root.b;

public class TV {

    private String location;

    public TV(String location) {
        this.location = location;
    }

    public void turnTVon() {
        System.out.println("Включаем телевизор");
    }

    public void turnTVOff() {
        System.out.println("Выключаем телевизор");
    }
}
