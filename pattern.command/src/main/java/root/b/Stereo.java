package root.b;

public class Stereo {

    private int volume = 0;
    private String room;

    public Stereo(String room) {
        this.room = room;
    }

    public void on() {
        System.out.println("stereo on");
    }

    public void off() {
        System.out.println("Stereo off");
    }

    public void setCD() {
        System.out.println("CD is set");
    }

    public void setDVD() {
        System.out.println("DVD is set");
    }

    public void setRadio() {
        System.out.println("radio is set");
    }

    public void setVolume(int volume) {
        this.volume = volume;
        System.out.println("volume is set to " + volume);
    }

}
