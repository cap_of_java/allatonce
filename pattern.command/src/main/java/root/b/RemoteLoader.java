package root.b;

public class RemoteLoader {

    public static void main(String[] args) {
        RemoteControl remoteControl = new RemoteControl();

        Light livingRoomLight = new Light("Living Room");
        Light kitchenLight = new Light("Kitchen");
        CeilingFan ceilingFan = new CeilingFan("Living Room");

        TV tv = new TV("Living Room");
        Hottub hottub = new Hottub();
        GarageDoor garageDoor = new GarageDoor("");
        Stereo stereo = new Stereo("Living Room");

        LightOnCommand livingRoomLightOnCommand = new LightOnCommand(livingRoomLight);
        LightOffCommand livingRoomLightOffCommand = new LightOffCommand(livingRoomLight);

        LightOnCommand kitchenLightOnCommand = new LightOnCommand(kitchenLight);
        LightOffCommand kitchenLightOffCommand = new LightOffCommand(kitchenLight);

        CeilingFanHighCommand ceilingFanHighCommand = new CeilingFanHighCommand(ceilingFan);
        CeilingFanMediumCommand ceilingFanMediumCommand = new CeilingFanMediumCommand(ceilingFan);
        CeilingFanOffCommand ceilingFanOffCommand = new CeilingFanOffCommand(ceilingFan);

        GarageDoorUpCommand garageDoorUpCommand = new GarageDoorUpCommand(garageDoor);
        GarageDoorDownCommand garageDoorDownCommand = new GarageDoorDownCommand(garageDoor);

        StereoOnWithCDCommand stereoOnWithCDCommand = new StereoOnWithCDCommand(stereo);
        StereoOffCommand stereoOffCommand = new StereoOffCommand(stereo);

        remoteControl.setCommand(0, livingRoomLightOnCommand, livingRoomLightOffCommand);
        remoteControl.setCommand(1, kitchenLightOnCommand, kitchenLightOffCommand);
        remoteControl.setCommand(2, ceilingFanHighCommand, ceilingFanOffCommand);
        remoteControl.setCommand(3, stereoOnWithCDCommand, stereoOffCommand);
        remoteControl.setCommand(4, ceilingFanMediumCommand, ceilingFanOffCommand);

        remoteControl.onButtonWasPush(0);
        remoteControl.offButtonWasPush(0);
        remoteControl.undoButtonWasPushed();
//        remoteControl.onButtonWasPush(1);
//        remoteControl.offButtonWasPush(1);
//
//        remoteControl.onButtonWasPush(2);
//        remoteControl.offButtonWasPush(2);
//
//        remoteControl.onButtonWasPush(3);
//        remoteControl.offButtonWasPush(3);

        remoteControl.onButtonWasPush(2);
        remoteControl.undoButtonWasPushed();
        remoteControl.onButtonWasPush(4);
        remoteControl.undoButtonWasPushed();


        TVOnCommand tvOnCommand = new TVOnCommand(tv);
        TVOffCommand tvOffCommand = new TVOffCommand(tv);
        HottubOnCommand hottubOnCommand = new HottubOnCommand(hottub);
        HottubOffCommand hottubOffCommand = new HottubOffCommand(hottub);

        Command[] partyOn = {livingRoomLightOnCommand, stereoOnWithCDCommand, tvOnCommand, hottubOnCommand};
        Command[] partyOff = {livingRoomLightOffCommand, stereoOffCommand, tvOffCommand, hottubOffCommand};
        MacroCommand partyOnCommand = new MacroCommand(partyOn);
        MacroCommand partyOffCommand = new MacroCommand(partyOff);

        remoteControl.setCommand(5, partyOnCommand, partyOffCommand);

        System.out.println("===== party on =====");
        remoteControl.onButtonWasPush(5);
        System.out.println("===== party off =====");
        remoteControl.offButtonWasPush(5);
    }

}
