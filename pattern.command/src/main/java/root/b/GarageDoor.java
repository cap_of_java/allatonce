package root.b;

public class GarageDoor {

    private String room;

    public GarageDoor(String room) {
        this.room = room;
    }

    public void up() {
        System.out.println("Ворота поднимаются");
    }

    public void down() {
        System.out.println("Ворота опускаются");
    }

    public void stop() {
        System.out.println("Вопрота остановились");
    }

    public void lightOn() {
        System.out.println("Свет в гараже включился");
    }

    public void lightOff() {
        System.out.println("Свет в гараже выключился");
    }

}
