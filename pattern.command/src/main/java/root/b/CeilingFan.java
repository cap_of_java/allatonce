package root.b;

public class CeilingFan {

    public static final int HIGH = 3;
    public static final int MEDIUM = 2;
    public static final int LOW = 1;
    public static final int OFF = 0;
    private int speed;
    private String location;

    public CeilingFan(String room) {
        this.location = room;
    }

    public void high() {
        this.speed = HIGH;
        printSpeed();
    }

    public void medium() {
        this.speed = MEDIUM;
        printSpeed();
    }

    public void low() {
        this.speed = LOW;
        printSpeed();
    }

    public void off() {
        this.speed = OFF;
        printSpeed();
    }

    public int getSpeed() {
        return speed;
    }

    void printSpeed() {
        System.out.println("Скорость вентилятора в " + location + " выставлена на " + speed);
    }

}
