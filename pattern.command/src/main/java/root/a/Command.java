package root.a;

public class Command {

    Receiver receiver;

    public Command(Receiver receiver) {
        this.receiver = receiver;
    }

    public void execute() {
        receiver.action1();
        receiver.action2();
    }

}
