package threads.creation.demo1;

import java.util.Random;

public class App {

    public static void main(String[] args) {
        System.out.println("Main thread name is: " + Thread.currentThread().getName());
        Runner runner = new Runner();
        runner.start();

        Runner runner2 = new Runner();
        runner2.start();
    }

}

class Runner extends Thread {

    @Override
    public void run() {
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            System.out.println("Hello " + i + " from " + Thread.currentThread().getName());
            try {
                Thread.sleep(random.nextInt(500));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}