package threads.synchronization;

import java.util.Scanner;

/**
 * For those wondering: when a variable is declared volatile, the value of the variable is read and written directly from the memory.
 * Each thread caches the values of variables from the memory.
 * Thus the thread does not have to always refer the memory, and it can simply read the values from the cache.
 * This is gives rise to the problem of visibility.
 * This cache may not be consistent with what other variables see.
 * With volatile, we skip the cache and read/write directly in the memory. Thus, changes are visible to all threads.
 *
 *
 * Unless volatile in Java does that on platforms that don't guarantee cache coherency, volatile doesn't do that.
 * If it's anything like C, it is a compiler/JIT directive that makes sure reads and writes to the variable are never optimized away.
 * "Skipping the cache and read/write directly in the memory" is not really a thing you can do on x64 as far as I know.
 * Volatile is one of the most misunderstood concepts in programming.
 *
 *
 * If I remember correctly, volatile variables are not read and written directly to the memory,
 * instead, read and write operations of such variables are done on a shared cache.
 *
 */
public class App {

    public static void main(String[] args) {
        Processor proc1 = new Processor();
        proc1.start();

        System.out.println("Press Enter to stop");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        proc1.shutdown();
    }

}

class Processor extends Thread {

    /**
     * этот код гарантированно будет работать благодаря ключевому слову volatile
     */
    private volatile boolean running = true;

    public void run() {
        while (running) {
            System.out.println("Hello");
            try {
                Thread.sleep(200L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown() {
        running = false;
    }

}
