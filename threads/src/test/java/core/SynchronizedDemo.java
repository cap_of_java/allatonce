package core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * synchronized ( Expression ) Block
 *
 * synchronized can be used on blocks of code
 * and methods
 *
 */
public class SynchronizedDemo {


    /**
     * The type of Expression must be a reference type
     */
    @Test
    void doIt() {
        synchronized ((Integer)(10 + 20)) {

        }
    }

    /**
     * The type of Expression must be a reference type
     */
    @Test
    void doIt2() {
        synchronized (getObject()) {

        }
    }

    private Object getObject() {
        return new Object();
    }

    /**
     * The type of Expression must be a reference type, or the compile error will occurs (occurs - имеет место быть)
     */
    @Test
    void doIt3() {
//        synchronized (1 + 1) {
//
//        }
    }

    /**
     * if the value of the Expression is null, a NullPointerException is
     * thrown.
     */
    @Test
    void doIt4() {
        Object obj = null;
        Assertions.assertThrows(NullPointerException.class, () -> {
            synchronized (obj) {
            }
        });
    }

    /**
     * If evaluation of the Expression completes abruptly for some reason, then the
     * synchronized statement completes abruptly for the same reason.
     */
    @Test
    void doIt5() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            synchronized (throwException()) {

            }
        });
    }

    public Object throwException() {
        throw new IllegalArgumentException("test");
    }

    /**
     * Otherwise, let the non-null value of the Expression be V. The executing thread
     * locks the monitor associated with V. Then the Block is executed, and then there
     * is a choice:
     *
     * – If execution of the Block completes normally, then the monitor is unlocked
     * and the synchronized statement completes normally
     */
    @Test
    void doIt6() {
        Object v = new Object();
        synchronized (v){

        }
    }

    /**
     * Otherwise, let the non-null value of the Expression be V. The executing thread
     * locks the monitor associated with V. Then the Block is executed, and then there
     * is a choice:
     *
     * If execution of the Block completes abruptly for any reason, then the monitor
     * is unlocked and the synchronized statement completes abruptly for the same
     * reason
     */
    @Test
    void doIt7() {
        Object v = new Object();
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            synchronized (v){
                throw new IllegalArgumentException("test7");
            }
        });
    }

    /**
     * The locks acquired by synchronized statements are the same as the locks that
     * are acquired implicitly by synchronized methods (§8.4.3.6). A single thread may
     * acquire a lock more than once
     *
     * реентерабельная (от англ. reentrant — повторно входимый),
     *
     * !!! Note that this program would deadlock if a single thread were not permitted to lock a
     * monitor more than once.
     */
    @Test
    void doIt8() {
        Object t = new Object();
        synchronized(t) {
            synchronized(t) {
                System.out.println("made it!");
            }
        }
    }

    /**
     * A synchronized method acquires a monitor before it executes.
     * For a class (static) method, the monitor associated with the Class object for the
     * method's class is used.
     * For an instance method, the monitor associated with this (the object for which the
     * method was invoked) is used.
     */
    @Test
    void doIt9() {
        // DemoSyncAsStatement and DemoSyncMethod classes have the same effect
    }

}

class DemoSyncAsStatement {
    int count;

    void increment() {
        synchronized (this) {
            count++;
        }
    }

    static int staticCount;

    static void staticIncrement() {
        synchronized (DemoSyncAsStatement.class) {
            staticCount++;
        }
    }
}

class DemoSyncMethod {
    int count;
    synchronized void bump() {
        count++;
    }
    static int classCount;
    static synchronized void classBump() {
        classCount++;
    }
}