package core;

import java.util.Scanner;

class Processor {

    public void produce() throws InterruptedException {
        synchronized (this) {
            System.out.println("Producer thread running...");
            wait();  // wait останавливает исполнение потока и делает unlock
            System.out.println("Producer resumed");
        }
    }

    public void consume() throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        Thread.sleep(2000);
        synchronized (this) {
            System.out.println("Waiting for return key.");
            scanner.nextLine();
            System.out.println("Return key pressed");
            notify();  // notify сообщает другим потокам, которые используют этот же монитор
            // что они могут продолжить свое исполнение, но при этом не делает unlock, что бы другие потоки могли продолжить исполняться
            // , должен произойти unlock, т.е. выход из синхронизованного блока
            System.out.println("Notify is called");
            Thread.sleep(4000);
            System.out.println("sleep is over");
        }
    }

}
