package core;

import org.junit.jupiter.api.Test;

import java.util.Scanner;

public class WaitAndNotifyDemo {

    @Test
    void doIt1() throws InterruptedException {
        Object obj = new Object();

//        wait(0,0) and wait(0) is equivalent to invocation of wait()
//        obj.wait();
//        obj.wait(0);
//        obj.wait(0, 0);
    }

    @Test
    void doIt2() throws InterruptedException {

    }

}

