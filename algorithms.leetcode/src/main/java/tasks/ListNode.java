package tasks;

import java.util.Objects;

public class ListNode {

    public int val;
    public ListNode next;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    public static ListNode toListNode(int... input) {
        ListNode result = new ListNode(input[0]);
        ListNode cursor = result;
        for (int i = 1; i < input.length; i++) {
            cursor.next = new ListNode(input[i]);
            cursor = cursor.next;
        }
        return result;
    }

    public static int size(ListNode list) {
        int size = 0;
        ListNode cursor = list;
        while (cursor != null) {
            size++;
            cursor = cursor.next;
        }
        return size;
    }

    public static int sum(ListNode list) {
        int sum = 0;
        ListNode cur = list;
        while (cur != null) {
            sum += cur.val;
            cur = cur.next;
        }
        return sum;
    }

    public static ListNode zip(ListNode l1, ListNode l2) {
        ListNode cur1 = l1;
        ListNode cur2 = l2;

        ListNode prev = null;
        while (cur1 != null && cur2 != null) {
            cur1.val = cur1.val + cur2.val;
            prev = cur1;
            cur1 = cur1.next;
            cur2 = cur2.next;
        }
        prev.next = cur1 != null ? cur1 : cur2;
        return l1;
    }

    @Override
    public String toString() {
        return "{" +
                "val:" + val +
                ", next:" + next +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListNode listNode = (ListNode) o;
        return val == listNode.val &&
                Objects.equals(next, listNode.next);
    }

    @Override
    public int hashCode() {
        return Objects.hash(val, next);
    }
}
