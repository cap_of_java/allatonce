package tasks.algorithms;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Given a string s, find the length of the longest substring without repeating characters.
 *
 * s consists of English letters, digits, symbols and spaces.
 */
public class N003_LongestSubstringWithoutRepeatingCharacters {

    @Test
    void doIt01() {
        assertThat(lengthOfLongestSubstring("abcabcbb"))
                .isEqualTo(3);
    }

    @Test
    void doIt02() {
        assertThat(lengthOfLongestSubstring("bbbbb"))
                .isEqualTo(1);
    }

    @Test
    void doIt03() {
        assertThat(lengthOfLongestSubstring("pwwkew"))
                .isEqualTo(3);
    }

    @Test
    void doIt04() {
        assertThat(lengthOfLongestSubstring(""))
                .isEqualTo(0);
    }

    @Test
    void doIt05() {
        assertThat(lengthOfLongestSubstring(" "))
                .isEqualTo(1);
    }

    @Test
    void doIt06() {
        assertThat(lengthOfLongestSubstring("au"))
                .isEqualTo(2);
    }

    @Test
    void doIt07() {
        assertThat(lengthOfLongestSubstring("dvdf"))
                .isEqualTo(3);
    }

    @Test
    void doIt08() {
        assertThat(lengthOfLongestSubstring("abba"))
                .isEqualTo(2);
    }

    @Test
    void doIt09() {
        assertThat(lengthOfLongestSubstring("tmmzuxt"))
                .isEqualTo(5);
    }

    public int lengthOfLongestSubstring(String s) {
        return brute2(s);
    }

    public int brute2(String str) {
        if (0 == str.length() || 1 == str.length()) {
            return str.length();
        }
        int[] window = new int[256];
        Arrays.fill(window, -1);
        char[] input = str.toCharArray();
        window[input[0]] = 0;
        int start, curL = 1;
        int maxL = Integer.MIN_VALUE;
        for (int i = 1; i < input.length; i++) {
            char ch = input[i];
            curL++;
            if (window[ch] != -1) {
                start = window[ch];
                curL -= start + 1;
            }
            if (curL > maxL) {
                maxL = curL;
            }
            window[ch] = i;
        }
        return Math.max(maxL, curL);
    }

    public int brute(String s) {
        int longest = 0;
        Map<Character, Integer> indices = new HashMap<>();
        char[] word = s.toCharArray();

        int currentLength = 0;
        for (int i = 0; i < word.length; i++) {
            char c = word[i];
            if (indices.containsKey(c)) {
                if (currentLength > longest) {
                    longest = currentLength;
                }
                currentLength = 0;
                int prev = indices.get(c);
                indices.clear();
                i = prev;
                continue;
            }
            currentLength++;
            indices.put(c, i);
        }
        return Math.max(longest, currentLength + 1);
    }

    /**
     * char range lies between 0 to 65,535 (inclusive)
     */
    @Test
    void doIt2() {
        int[] window = new int[256];
        Arrays.fill(window, -1);

        char[] input = "abcabcbb".toCharArray();

        int start = 0;
        int end = 0;
        int maxL = Integer.MIN_VALUE;
        for (int i = 0; i < input.length; i++) {
            char ch = input[i];
            end = i;
            if (window[ch] != -1) {
                int length = end - start + 2;
                if (maxL < length) {
                    maxL = length;
                }
                int last = window[ch];
                start = last + 1;
                window[ch] = i;
            } else {
                window[ch] = i;
            }
            System.out.println(maxL);
        }
    }




}
