package tasks.algorithms;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 * You can return the answer in any order.
 */
public class N001_TwoSum {

    @Test
    void doIt01() {
        int[] actual = call(new int[]{2, 7, 11, 15}, 9);
        assertThat(actual)
                .containsExactlyInAnyOrder(0, 1);
    }

    @Test
    void doIt02() {
        int[] actual = call(new int[]{3,2,4}, 6);
        assertThat(actual)
                .containsExactlyInAnyOrder(1, 2);
    }

    @Test
    void doIt03() {
        int[] actual = call(new int[]{3, 3}, 6);
        assertThat(actual)
                .containsExactlyInAnyOrder(0, 1);
    }

    @Test
    void doIt04() {
        int[] actual = call(new int[]{1, 3, 3, 4}, 6);
        assertThat(actual)
                .containsExactlyInAnyOrder(1, 2);
    }

    @Test
    void doIt05() {
        int[] actual = call(new int[]{0, 4, 3, 0}, 0);
        assertThat(actual)
                .containsExactlyInAnyOrder(0, 3);
    }

    public int[] call(int[] nums, int target) {
        return twoSum(nums, target);
//        return twoSumBrute(nums, target);
//        return twoSumOnePass(nums, target);
    }

    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> valueToIdx = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {


            if (valueToIdx.containsKey(nums[i])) {
                int idx = valueToIdx.get(nums[i]);
                if (idx == i) {
                    continue;
                }
                return new int[]{idx, i};
            }
            if (nums[i] < target) {
                int left = target - nums[i];
                valueToIdx.put(left, i);
            }
        }
        return null;
    }

    public int[] twoSumBrute(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    return new int[]{i, j};
                }
            }
        }
        throw new IllegalArgumentException("No two sum for you");
    }

    public int[] twoSumOnePass(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int complement = target - nums[i];
            if (map.containsKey(complement)) {
                return new int[]{map.get(complement), i};
            }
            map.put(nums[i], i);
        }
        throw new IllegalArgumentException("No two sum for you");
    }

}
