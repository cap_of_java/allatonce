package tasks.algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Given a signed 32-bit integer x, return x with its digits reversed.
 * If reversing x causes the value to go outside the signed 32-bit integer range,
 * then return 0.
 *
 * Assume the environment does not allow you to store 64-bit integers (signed or unsigned).
 *
 * Constraints
 *-2^31 <= x <= 2^31 - 1
 */
public class N007_ReverseInteger {

    private int attempt01(int x) {
        return 0;
    }

    public int reverse(int x) {
        return attempt01(x);
    }

    @Test
    void test01() {
        Assertions.assertEquals(321, reverse(123));
    }

    @Test
    void test02() {
        Assertions.assertEquals(-321, reverse(-123));
    }

    @Test
    void test03() {
        Assertions.assertEquals(21, reverse(120));
    }

    @Test
    void test04() {
        Assertions.assertEquals(0, reverse(0));
    }

}
