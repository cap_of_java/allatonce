package tasks.algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tasks.ListNode;

import static tasks.ListNode.toListNode;

/**
 * You are given two non-empty linked lists representing two non-negative integers.
 * The digits are stored in reverse order, and each of their nodes contains a single digit.
 * Add the two numbers and return the sum as a linked list.
 * <p>
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 */
public class N002_AddTwoNumbers {

    @Test
    void doIt01() {
        ListNode l1 = toListNode(2, 4, 3);
        ListNode l2 = toListNode(5, 6, 4);

        ListNode expected = toListNode(7, 0, 8);
        ListNode actual = addTwoNumbers(l1, l2);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void doIt02() {
        ListNode l1 = toListNode(5, 5, 3);
        ListNode l2 = toListNode(5, 5, 4);

        ListNode expected = toListNode(0, 1, 8);
        ListNode actual = addTwoNumbers(l1, l2);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void doIt9() {
        ListNode l1 = toListNode(5, 5, 3);
        ListNode l2 = toListNode(5, 5, 4);
        Assertions.assertEquals(toListNode(0, 1, 8), addTwoNumbers(l1, l2));
    }


    @Test
    void doIt10() {
        ListNode l1 = toListNode(5, 5, 3);
        ListNode l2 = toListNode(5, 5, 4, 4, 6);
        Assertions.assertEquals(toListNode(0, 1, 8, 4, 6), addTwoNumbers(l1, l2));
    }

    @Test
    void doIt11() {
        ListNode l1 = toListNode(5, 5, 3, 4, 6);
        ListNode l2 = toListNode(5, 5, 4);
        Assertions.assertEquals(toListNode(0, 1, 8, 4, 6), addTwoNumbers(l1, l2));
    }

    @Test
    void doIt12() {
        ListNode l1 = toListNode(5, 5, 5);
        ListNode l2 = toListNode(5, 4, 4, 4, 6);
        Assertions.assertEquals(toListNode(0, 0, 0, 5, 6), addTwoNumbers(l1, l2));
    }

    @Test
    void doIt13() {
        ListNode l1 = toListNode(5, 5, 5, 4, 6);
        ListNode l2 = toListNode(5, 4, 4);
        Assertions.assertEquals(toListNode(0, 0, 0, 5, 6), addTwoNumbers(l1, l2));
    }

    @Test
    void doIt14() {
        ListNode l1 = toListNode(0);
        ListNode l2 = toListNode(0);
        Assertions.assertEquals(toListNode(0), addTwoNumbers(l1, l2));
    }

    @Test
    void doIt15() {
        ListNode l1 = toListNode(9, 9, 9, 9, 9, 9, 9);
        ListNode l2 = toListNode(9, 9, 9, 9);
        Assertions.assertEquals(toListNode(8, 9, 9, 9, 0, 0, 0, 1), addTwoNumbers(l1, l2));
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        return leetCode(l1, l2);
//        return brute1(l1, l2);
//        return brute2(l1, l2);
    }

    private ListNode brute1(ListNode l1, ListNode l2) {
        ListNode cur1 = l1;
        ListNode cur2 = l2;

        ListNode prev = null;
        int extra = 0;
        while (cur1 != null && cur2 != null) {
            int value = cur1.val + cur2.val + extra;
            if (value / 10 != 0) {
                value = value % 10;
                extra = 1;
            } else {
                extra = 0;
            }
            cur1.val = value;
            prev = cur1;
            cur1 = cur1.next;
            cur2 = cur2.next;
        }
        ListNode end = cur1 != null ? cur1 : cur2;
        prev.next = end;

        while (end != null) {
            int value = end.val + extra;
            if (value / 10 != 0) {
                value = value % 10;
                extra = 1;
            } else {
                extra = 0;
            }
            end.val = value;
            prev = end;
            end = end.next;
        }
        if (extra != 0) {
            prev.next = new ListNode(extra);
        }
        return l1;
    }

    private ListNode brute2(ListNode l1, ListNode l2) {
        ListNode cur1 = l1;
        ListNode cur2 = l2;

        ListNode dummyHead = new ListNode(0);
        ListNode cursor = dummyHead;
        int carry = 0;
        while (cur1 != null || cur2 != null) {
            int val1 = cur1 == null ? 0 : cur1.val;
            int val2 = cur2 == null ? 0 : cur2.val;

            int value = val1 + val2 + carry;
            if (value / 10 != 0) {
                value = value % 10;
                carry = 1;
            } else {
                carry = 0;
            }
            cursor.next = new ListNode(value);
            cursor = cursor.next;

            cur1 = cur1 == null ? null : cur1.next;
            cur2 = cur2 == null ? null : cur2.next;
        }
        if (carry == 1) {
            cursor.next = new ListNode(1);
        }
        return dummyHead.next;
    }

    private ListNode leetCode(ListNode l1, ListNode l2) {
        ListNode dummyHead = new ListNode(0);
        ListNode p = l1, q = l2, cur = dummyHead;
        int carry = 0;
        while (p != null || q != null) {
            int x = p != null ? p.val : 0;
            int y = q != null ? q.val : 0;
            int sum = carry + x + y;
            carry = sum / 10;
            cur.next = new ListNode(sum % 10);
            cur = cur.next;
            if (p != null) {
                p = p.next;
            }
            if (q != null) {
                q = q.next;
            }
        }
        if (carry > 0) {
            cur.next = new ListNode(carry);
        }
        return dummyHead.next;
    }

}
