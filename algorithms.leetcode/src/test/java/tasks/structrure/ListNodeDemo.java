package tasks.structrure;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tasks.ListNode;

import static tasks.ListNode.*;

public class ListNodeDemo {

    @Test
    void size1() {
        Assertions.assertEquals(size(toListNode(1)), 1);
    }

    @Test
    void size2() {
        Assertions.assertEquals(size(toListNode(1,2)), 2);
    }

    @Test
    void size5() {
        Assertions.assertEquals(size(toListNode(1,2,3,4,5)), 5);
    }

    @Test
    void sum01() {
        ListNode input = toListNode(1, 2, 3, 4, 5);
        Assertions.assertEquals(15, sum(input));
    }

    @Test
    void sum02() {
        ListNode input = toListNode(5);
        Assertions.assertEquals(5, sum(input));
    }

    @Test
    void zip01() {
        ListNode left = toListNode(1);
        ListNode right = toListNode(2);
        Assertions.assertEquals(toListNode(3), zip(left, right));
    }

    @Test
    void zip02() {
        ListNode left = toListNode(1, 3);
        ListNode right = toListNode(5, 7);
        Assertions.assertEquals(toListNode(6, 10), zip(left, right));
    }

    @Test
    void zip03() {
        ListNode left = toListNode(1);
        ListNode right = toListNode(5, 7);
        Assertions.assertEquals(toListNode(6, 7), zip(left, right));
    }

    @Test
    void zip04() {
        ListNode left = toListNode(1, 3);
        ListNode right = toListNode(5);
        Assertions.assertEquals(toListNode(6, 3), zip(left, right));
    }

    @Test
    void zip05() {
        ListNode left = toListNode(-1, 3, 10);
        ListNode right = toListNode(5);
        Assertions.assertEquals(toListNode(4, 3, 10), zip(left, right));
    }

}
