package tasks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class ListNodeTask {

    @Test
    void doIt() {
        int[][] input = new int[][]{{1, 4, 5}, {1, 3, 4}, {2, 6}};
        int[] expected = new int[]{1, 1, 2, 3, 4, 4, 5, 6};

        ListNode[] listNodes = new ListNode[input.length];
        for (int i = 0; i < input.length; i++) {
            listNodes[i] = toListNode(input[i]);
        }

        ListNode actual = mergeKLists(listNodes);
        Assertions.assertEquals(toListNode(expected), actual);
    }

    @Test
    void doIt2() {
        int[] expected = new int[]{1, 1, 2, 3, 4, 4, 5, 6};

        ListNode list = toListNode(expected);

        Iterator<ListNode> iterator = getIterator(list);

        while (iterator.hasNext()) {
            System.out.println(iterator.next().val);
        }

    }

    public ListNode toListNode(int[] ints) {
        ListNode head = null;
        ListNode current = null;
        for (int i : ints) {
            if (head == null) {
                head = new ListNode(i);
                current = head;
            } else {
                current.next = new ListNode(i);
                current = current.next;
            }
        }
        return head;
    }

    public ListNode mergeKLists(ListNode[] lists) {
        NavigableMap<Integer, Integer> map = new TreeMap<>();
        for (ListNode list : lists) {

            Iterator<ListNode> iterator = getIterator(list);
            while (iterator.hasNext()) {
                ListNode node = iterator.next();
                int val = node.val;
                if (!map.containsKey(val)) {
                    map.put(val, 1);
                } else {
                    map.put(val, map.get(val) + 1);
                }
            }

        }
        return toListNode(map);
    }

    public ListNode toListNode(NavigableMap<Integer, Integer> map) {
        ListNode head = null;
        ListNode current = null;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            for (int i = 0; i < entry.getValue(); i++) {
                if (head == null) {
                    head = new ListNode(entry.getKey());
                    current = head;
                } else {
                    current.next = new ListNode(entry.getKey());
                    current = current.next;
                }
            }
        }
        return head;
    }


    public Iterator<ListNode> getIterator(ListNode listNode) {
        return new Iterator<ListNode>() {

            ListNode current = listNode;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public ListNode next() {
                ListNode res = current;
                current = res.next;
                return res;
            }
        };
    }

}
