package core;

import java.util.Arrays;

public class Recursion {

    public static long factorial(long limit) {
        if (limit == 0 || limit == 1) {
            return 1;
        }
        return limit * factorial(limit - 1);
    }

    public static long neverEnding() {
        return neverEnding();
    }

    public static int sum(int[] list) {
        if (list == null || list.length == 0) {
            return 0;
        }
        if (list.length == 1) {
            return list[0];
        }
        return list[list.length - 1] + sum(Arrays.copyOfRange(list, 0, list.length - 1));
    }

    public static int count(int[] list) {
        if (list.length == 0) {
            return 0;
        }
        return 1 + count(Arrays.copyOfRange(list, 0, list.length - 1));
    }
}
