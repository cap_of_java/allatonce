package core;

public class SelectionSort {

    private final Sortable[] list;

    public SelectionSort(Sortable[] list) {
        this.list = list;
    }

    public Sortable[] reverse() {
        Sortable[] result = new Sortable[list.length];
        for (int i = 0; i < list.length; i++) {

            Sortable highest = null;
            int highestIdx = -1;
            for (int j = 0; j < list.length; j++) {
                Sortable item = list[j];
                if (item != null) {
                    if (highest == null) {
                        highest = item;
                        highestIdx = j;
                    } else {
                        if (highest.value() > item.value()) {
                            highest = item;
                            highestIdx = j;
                        }
                    }
                }
            }
            list[highestIdx] = null;
            result[i] = highest;
        }
        return result;
    }

    public Sortable[] sort() {
        Sortable[] result = new Sortable[list.length];
        for (int i = 0; i < list.length; i++) {

            Sortable highest = null;
            int highestIdx = -1;
            for (int j = 0; j < list.length; j++) {
                Sortable item = list[j];
                if (item != null) {
                    if (highest == null) {
                        highest = item;
                        highestIdx = j;
                    } else {
                        if (highest.value() < item.value()) {
                            highest = item;
                            highestIdx = j;
                        }
                    }
                }
            }
            list[highestIdx] = null;
            result[i] = highest;
        }
        return result;
    }

    public Sortable[] getList() {
        return list;
    }
}
