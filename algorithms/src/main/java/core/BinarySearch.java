package core;


public class BinarySearch {

    private final int[] list;
    private final int item;

    public BinarySearch(int[] list, int item) {
        this.list = list;
        this.item = item;
    }

    public int search() {
        int low = 0;
        int high = list.length - 1;

        while (low <= high) {
            int mid = (low + high) / 2;
            int guess = list[mid];

            if (guess < item) {
                low = mid+1;
            }else if (guess > item) {
                high = mid-1;
            } else {
                return mid;
            }
        }
        return -1;
    }

}
