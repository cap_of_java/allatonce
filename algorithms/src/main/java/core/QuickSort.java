package core;

import java.util.Arrays;

public class QuickSort {


    public Integer[] sort(Integer[] list) {
        if (list.length < 2) {
            return list;
        }
        Integer pick = list[0];

        Integer[] less = new Integer[list.length];
        int lessSize = 0;
        Integer[] greater = new Integer[list.length];
        int greaterSize = 0;
        for (Integer item : list) {
            if (item > pick) {
                greater[greaterSize] = item;
                greaterSize++;
            } else {
                less[lessSize] = item;
                lessSize++;
            }
        }
        return null;
    }

    public Integer[] concat(Integer[] left, int leftSize, Integer middle, Integer[] right, int rightSize) {
        Integer[] result = new Integer[leftSize + 1 + rightSize];
        System.arraycopy(left, 0, result, 0, leftSize);
        result[leftSize] = middle;
        System.arraycopy(right, 0, result, leftSize + 1, rightSize);
        return result;
    }


}
