package core.queue;

/**
 *
 * FILO - First In Last Out
 */
public interface Stack<T> {

    void push(T item);

    T pop();

    boolean isEmpty();

}
