package core.queue;

import java.util.ArrayList;
import java.util.List;

public class SimpleStack<T> implements Stack<T> {

    private final List<T> stack = new ArrayList<>();

    @Override
    public void push(T item) {
        stack.add(0, item);
    }

    @Override
    public T pop() {
        return stack.remove(0);
    }

    @Override
    public boolean isEmpty() {
        return stack.isEmpty();
    }
}
