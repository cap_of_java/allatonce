package core.queue;

/**
 * Первый пришел, первый ушел
 * FIFO - First In, First Out
 */
public interface Queue<T> {

    void add(T item);

    T remove();

    boolean isEmpty();

}
