package core.trees;

import core.queue.SimpleQueue;
import core.queue.SimpleStack;

public class Binary {

    public static void main(String[] args) {
        Tree root = new Tree(20,
                new Tree(7,
                        new Tree(4, null, new Tree(6)), new Tree(9)),
                new Tree(35,
                        new Tree(31, new Tree(28), null),
                        new Tree(40, new Tree(38), new Tree(52))));


        // System.out.println(root.sumRecursive());
//        System.out.println(Tree.sumDeep(root));
        System.out.println(Tree.sumWide(root));
    }

    static class Tree {
        int value;
        Tree left;
        Tree right;

        public Tree(int value) {
            this.value = value;
        }

        public Tree(int value, Tree left, Tree right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }

        /**
         * 1. Стек вызовов имеет меньший размер чем обычная память
         * И имеет свойство заканчиваться раньше, а так же в случае больших деревьев появляется вероятность выйти за пределы доступного размера стека вызовава
         * 2. вызов функции само по себе является затратной операцией, стек используется не только для адресов возврата, но и для локальных переменных, параметров вызова
         * это приводит к тому что рекурсивный обход обычно будет медленнее, чем итеративный подход
         */
        public int sumRecursive() {
            int summ = value;
            if (left != null) {
                summ += left.sumRecursive();
            }
            if (right != null) {
                summ += right.sumRecursive();
            }
            return summ;
        }

        public static int sumDeep(Tree root) {
            SimpleStack<Tree> stack = new SimpleStack<>();
            stack.push(root);

            int summ = 0;

            while (!stack.isEmpty()) {
                Tree node = stack.pop();
                System.out.println(node.value);
                summ += node.value;

                if (node.right != null) {
                    stack.push(node.right);
                }
                if (node.left != null) {
                    stack.push(node.left);
                }
            }
            return summ;
        }

        public static int sumWide(Tree root) {
            SimpleQueue<Tree> stack = new SimpleQueue<>();
            stack.add(root);

            int summ = 0;

            while (!stack.isEmpty()) {
                Tree node = stack.remove();
                System.out.println(node.value);
                summ += node.value;

                if (node.right != null) {
                    stack.add(node.right);
                }
                if (node.left != null) {
                    stack.add(node.left);
                }
            }
            return summ;
        }

    }

}
