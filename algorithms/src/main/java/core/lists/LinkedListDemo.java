package core.lists;


import java.util.Iterator;

public class LinkedListDemo {

    public static void main(String[] args) {

    }

    static class LinkedList<T> implements Iterable<T>{
        ListItem<T> head;
        ListItem<T> tail;

        public LinkedList() {
        }

        public void addToEnd(T item) {
            ListItem<T> newItem = new ListItem<>(item);
            if (isEmpty()) {
                head = newItem;
                tail = newItem;
            } else {
                tail.next = newItem;
                tail = newItem;
            }
        }

        public void reverse() {
            if (!isEmpty() && head.next != null) {
                tail = head;
                ListItem<T> current = head.next;
                head.next = null;
                while (current != null) {
                    ListItem<T> next = current.next;
                    
                }
            }
        }


        public boolean isEmpty() {
            return head == null;
        }

        @Override
        public Iterator<T> iterator() {
            return new Iterator<T>() {
                ListItem<T> current = head;

                @Override
                public boolean hasNext() {
                    return current != null;
                }

                @Override
                public T next() {
                    T data = current.data;
                    current = current.next;
                    return data;
                }
            };
        }
    }

    static class ListItem<T> {
        T data;
        ListItem<T> next;
        public ListItem(T data) {
            this.data = data;
        }
    }

}


