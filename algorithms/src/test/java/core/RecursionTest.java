package core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static core.Recursion.factorial;

public class RecursionTest {

    @Test
    void fact0() {
        Assertions.assertEquals(1, factorial(0));
    }

    @Test
    void fact1() {
        Assertions.assertEquals(1, factorial(1));
    }

    @Test
    void fact3() {
        Assertions.assertEquals(6, factorial(3));
    }

    @Test
    void fact4() {
        Assertions.assertEquals(24, factorial(4));
    }

    @Test
    void fact16() {
        Assertions.assertEquals(20922789888000L, factorial(16));
    }

    @Test
    void infinite() {
        Assertions.assertThrows(Error.class, Recursion::neverEnding);
    }

    @Test
    void sum() {
        int[] list = {1, 5, 10, 1000, -1, 0, 500};
        Assertions.assertEquals(1515, Recursion.sum(list));
    }

    @Test
    void count() {
        int[] list = {1, 5, 10, 1000, -1, 0, 500};
        Assertions.assertEquals(7, Recursion.count(list));
    }

}
