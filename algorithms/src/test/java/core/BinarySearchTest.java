package core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BinarySearchTest {

    @Test
    void test() {
        int[] array = {};
        int target = 0;
        Assertions.assertEquals(-1, new BinarySearch(array, target).search());
    }

    @Test
    void test1() {
        int[] array = {1, 2, 3, 4, 5};
        int target = 4;
        Assertions.assertEquals(3, new BinarySearch(array, target).search());
    }

    @Test
    void test2() {
        int[] array = {1, 2, 3, 4, 5};
        int target = 10;
        Assertions.assertEquals(-1, new BinarySearch(array, target).search());
    }

    @Test
    void test3() {
        int[] array = {1, 5, 6, 7, 9, 10, 100, 1001, 1002};
        int target = 1001;
        Assertions.assertEquals(7, new BinarySearch(array, target).search());
    }

    @Test
    void test4() {
        int[] array = {1, 2, 2, 2, 2, 3, 5, 1001, 1002, 1999};
        int target = 5;
        Assertions.assertEquals(6, new BinarySearch(array, target).search());
    }

    @Test
    void test5() {
        int[] array = {1, 2, 2, 2, 2, 3, 5, 1001, 1002, 1999};
        int target = 10;
        Assertions.assertEquals(-1, new BinarySearch(array, target).search());
    }

}
