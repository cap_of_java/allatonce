package core.structures;

import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;
import java.util.Stack;

public class SimpleStackTest {

    @Test
    void doIt() {
        SimpleStack<Integer> stack = new SimpleStack<>();

        stack.push(1);
        stack.push(2);
        stack.push(3);

        System.out.println(stack);

        stack.pop();
        stack.pop();
        System.out.println(stack);
        stack.pop();
        System.out.println(stack);
        System.out.println(stack);

    }

    @Test
    void doIt2() {
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        System.out.println(stack);

        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
    }

}


class SimpleStack<T> {

    private Item<T> head;

    public void push(T data) {
        if (head == null) {
            head = new Item<>(data);
        } else {
            head = new Item<>(data, head);
        }
    }

    public T pop() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        T data = head.value;
        head = head.item;
        return data;
    }

    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public String toString() {
        return "{" +
                "head:" + head +
                '}';
    }

    private static class Item<T> {

        T value;
        Item<T> item;

        public Item(T value) {
            this.value = value;
        }

        public Item(T value, Item<T> item) {
            this.value = value;
            this.item = item;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }

}
