package core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class QuickSortTest {

    @Test
    void test() {
        Integer[] list = new Integer[] {5, 2, 1, 6, 8, 9, 110};
        Integer[] expected = new Integer[] {1, 2, 5, 6, 8, 9, 110};

        Integer[] result = new QuickSort().sort(list);
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void doIt() {

    }

}
