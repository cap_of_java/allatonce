package core;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Дан список интов, повторяющихся элементов в списке нет.
 * Нужно преобразовать это множество в строку,
 * сворачивая соседние по числовому ряду числа в диапазоны.
 */
class ListSummaryRangesIntoRanges {

    @Test
    void doIt1() {
        check(new int[]{1, 4, 5, 2, 3, 9, 8, 11, 0}, "0-5,8-9,11");
    }

    @Test
    void doIt2() {
        check(new int[]{1, 4, 3, 2}, "1-4");
    }

    @Test
    void doIt3() {
        check(new int[]{1, 4}, "1,4");
    }

    @Test
    void doIt4() {
        check(new int[]{}, "");
    }

    @Test
    void doIt5() {
        check(new int[]{-1}, "-1");
    }

    @Test
    void doIt6() {
        check(new int[]{0}, "0");
    }

    @Test
    void doIt7() {
        check(new int[]{0, 1, 2, 4, 5, 7}, "0-2,4-5,7");
    }

    @Test
    void doIt8() {
        check(new int[]{0, 2, 3, 4, 6, 8, 9}, "0,2-4,6,8-9");
    }


    private static void check(int[] input, String expected) {
        String result = fold(input);
        assertThat(result).isEqualTo(expected);
    }


    /**
     * Должна быть сортировка + 1 проход
     *
     * у меня получилось сортировка + 3 прохода
     */
    private static String fold(int[] nums) {
        if (nums.length == 0) {
            return "";
        }
        if (nums.length == 1) {
            return String.valueOf(nums[0]);
        }

        TreeSet<Integer> values = new TreeSet<>();
        for (int i : nums) {
            values.add(i);  // N * log N
        }

        LinkedList<LinkedList<Integer>> ranges = new LinkedList<>();

        Iterator<Integer> allNums = values.iterator();
        int cur = allNums.next();

        LinkedList<Integer> range = new LinkedList<>();
        range.addLast(cur);  // const
        ranges.add(range);  // const

        while (allNums.hasNext()) {  // 2 * N * log N
            int next = allNums.next();

            if (cur + 1 == next) {
                ranges.getLast().addLast(next);
            } else {
                LinkedList<Integer> newRange = new LinkedList<>();
                newRange.addLast(next);
                ranges.addLast(newRange);
            }
            cur = next;
        }

        return ranges.stream()  // 3 * N * log N
                .map(list -> list.size() == 1 ? String.valueOf(list.peekFirst()) : list.peekFirst() + "-" + list.peekLast())
                .collect(Collectors.joining(","));
    }
}
