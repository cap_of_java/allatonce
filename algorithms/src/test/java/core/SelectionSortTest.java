package core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Objects;

public class SelectionSortTest {

    @Test
    void sort() {
        Sortable[] list = new Sortable[]{
                new Music("THE BLACK KEYS", 35),
                new Music("RADIOHEAD", 156),
                new Music("NEUTRAL MILK HOTEL", 94),
                new Music("BECK", 88),
                new Music("WILCO", 111),
                new Music("THE STROKES", 61),
                new Music("KISHORE KUMAR", 141),
        };

        Sortable[] expected = new Sortable[]{
                new Music("RADIOHEAD", 156),
                new Music("KISHORE KUMAR", 141),
                new Music("WILCO", 111),
                new Music("NEUTRAL MILK HOTEL", 94),
                new Music("BECK", 88),
                new Music("THE STROKES", 61),
                new Music("THE BLACK KEYS", 35),
        };

        Sortable[] result = new SelectionSort(list).sort();
        Assertions.assertArrayEquals(expected, result);
    }


    @Test
    void reverse() {
        Sortable[] list = new Sortable[]{
                new Music("THE BLACK KEYS", 35),
                new Music("RADIOHEAD", 156),
                new Music("NEUTRAL MILK HOTEL", 94),
                new Music("BECK", 88),
                new Music("WILCO", 111),
                new Music("THE STROKES", 61),
                new Music("KISHORE KUMAR", 141),
        };

        Sortable[] expected = new Sortable[]{
                new Music("THE BLACK KEYS", 35),
                new Music("THE STROKES", 61),
                new Music("BECK", 88),
                new Music("NEUTRAL MILK HOTEL", 94),
                new Music("WILCO", 111),
                new Music("KISHORE KUMAR", 141),
                new Music("RADIOHEAD", 156),
        };

        Sortable[] result = new SelectionSort(list).reverse();
        Assertions.assertArrayEquals(expected, result);
    }

}

class Music implements Sortable {

    public String name;
    public int counter;

    public Music(String name, int counter) {
        this.name = name;
        this.counter = counter;
    }

    @Override
    public int value() {
        return counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Music music = (Music) o;
        return counter == music.counter &&
                Objects.equals(name, music.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, counter);
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", counter=" + counter +
                '}';
    }
}
