package com.methodreference;

import java.util.stream.Stream;

public class MethodReferenceDemo {

    public static void main(String[] args) {
        Stream.of(3, 4, 1, 5, 7, 2)
                .forEach(x -> System.out.print(x));
        Stream.of(1, 2, 5, 7)
                .forEach(System.out::print);
        System.out.println();
        Stream.generate(Math::random)
                .limit(10)
                .forEach(System.out::println);

    }
}
