package com.runnable;

public class RunnableDemoLambda {

    public static void main(String[] args) {
        new Thread(() -> System.out.println("Runnable inside lambda expression"))
                .start();
    }

}
