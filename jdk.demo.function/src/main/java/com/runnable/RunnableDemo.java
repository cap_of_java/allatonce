package com.runnable;

public class RunnableDemo {

    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Runnable inside an anonymous class");
            }
        }).start();
    }

}
