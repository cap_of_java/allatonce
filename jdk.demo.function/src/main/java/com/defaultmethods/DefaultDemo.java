package com.defaultmethods;

public interface DefaultDemo {

    int NUMBER = 19;

    static int add(int i, int j) {
        return i + j;
    }

}
