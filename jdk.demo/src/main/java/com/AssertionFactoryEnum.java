package com;

public enum AssertionFactoryEnum {

    EQUALS("равно", (x, y, z) -> {

    }),
    EQUALS1("почти равно", (x, y, z) -> {

    }),
    EQUALS2("не равно числу", (x, y, z) -> {

    }),
    EQUALS3("равно числу", (x, y, z) -> {

    }),
    EQUALS4("меньше", (x, y, z) -> {

    }),
    EQUALS5("больше", (x, y, z) -> {

    }),
    EQUALS6("не пусто", (x, y, z) -> {

    }),

    EQUALS7("не пусто2", (x, y, z) -> {

    }),

    EQUALS8("не пусто3", (x, y, z) -> {

    }),

    EQUALS9("не пусто4", (x, y, z) -> {

    });

    private String name;
    private Assertion assertion;

    AssertionFactoryEnum(String name, Assertion assertion) {
        this.name = name;
        this.assertion = assertion;
    }

    public interface Assertion {
        void compare(String firstValue, String secondValue, String message);
    }

    public static Assertion forType(String type) {
        String trimmed = type.trim().toLowerCase();
        return AssertionFactoryEnum.valueOf(trimmed).assertion;
    }

}
