package com;

import java.util.HashMap;
import java.util.Map;

public class AssertionFactorySwitch {

    public interface Assertion {
        void compare(String firstValue, String secondValue, String message);
    }

    private static AssertionFactorySwitch instance = new AssertionFactorySwitch();

    private AssertionFactorySwitch() {
    }

    public static Assertion forType(String type) {
        if (type == null) {
            throw new IllegalArgumentException("");
        }
        String trimmed = type.trim().toLowerCase();
        switch (trimmed) {
            case "равно":
                return instance.equals;
            case "не равно":
                return instance.equalsCaseSensitive;
            case "почти равно":
                return instance.notEqual;
            case "не равно числу":
                return instance.moreThan;
            case "равно числу":
                return instance.moreThan;
            case "меньше":
                return instance.moreThan;
            case "больше":
                return instance.moreThan;
            case "не пусто":
                return instance.moreThan;
            case "не пусто2":
                return instance.moreThan;
            case "не пусто3":
                return instance.moreThan;
            case "не пусто4":
                return instance.moreThan;
            default:
                throw new IllegalArgumentException("");
        }
    }

    private Assertion equals = (first, second, message) -> {
    };

    private Assertion equalsCaseSensitive = (first, second, message) -> {
    };

    private Assertion notEqual = (first, second, message) -> {
    };

    private Assertion moreThan = (first, second, message) -> {
    };

}
