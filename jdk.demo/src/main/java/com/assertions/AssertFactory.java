package com.assertions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AssertFactory {

    private final static AssertFactory INSTANCE = new AssertFactory();
    private final Map<String, Assertion> map = new HashMap<>();

    private AssertFactory() {
        map.put("не равно", (f, s) -> "");
        map.put("равно", (f, s) -> trim(f).equals(trim(s)) ? null : "sdfsfd");
    }

    public static Assertion forType(String type) {
        if (type == null) {
            throw new RuntimeException();
        }
        return INSTANCE.map.get(type.trim().toLowerCase());
    }

    public static String errorMsg(List<String> print) {
        return "";
    }

    private String trim(String str) {
        return str.trim().replace("\n", "").replace("\r", "");
    }

}
