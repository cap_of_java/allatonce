package com.assertions;


@FunctionalInterface
public interface Assertion {

    String compare(String first, String second);

}
