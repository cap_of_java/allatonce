package com.assertionswitch;

import java.util.Optional;
import java.util.function.BiFunction;

public interface StringAssertion extends BiFunction<String,String, Optional<String>> {

    static StringAssertion forType(String type) {
        if (type == null) {
            throw new RuntimeException();
        }
        String purifiedType = type.trim().toLowerCase();
        switch (purifiedType) {
            case "равно":
                return (x, y) -> Optional.empty();
            case "не равно":
                return (x, y) -> Optional.of("second");
            default:
                throw new RuntimeException("");
        }
    }

    private static String fo() {
        return "";
    }
    
}
