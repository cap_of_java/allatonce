package com;

import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;

public class AssertionFactory {

    public interface Assertion {
        void compare(String firstValue, String secondValue, String message);
    }

    private Map<String, Assertion> assertions = new IdentityHashMap<>(21);
//    private Map<String, Assertion> assertions = new HashMap<>();

    private static AssertionFactory instance = new AssertionFactory();

    private AssertionFactory() {
        assertions.put("равно", equals);
        assertions.put("не равно", equalsCaseSensitive);
        assertions.put("почти равно", notEqual);
        assertions.put("не равно числу", moreThan);
        assertions.put("равно числу", moreThan);
        assertions.put("меньше", moreThan);
        assertions.put("больше", moreThan);
        assertions.put("не пусто", moreThan);
        assertions.put("не пусто2", moreThan);
        assertions.put("не пусто3", moreThan);
        assertions.put("не пусто4", moreThan);
    }

    public static Assertion forType(String type) {
        if (type == null) {
            throw new IllegalArgumentException("");
        }
        String trimmed = type.trim().toLowerCase();
        if (instance.assertions.containsKey(trimmed)) {
            return instance.assertions.get(trimmed);
        } else {
            throw new IllegalArgumentException("");
        }
    }

    private Assertion equals = (first, second, message) -> {
    };

    private Assertion equalsCaseSensitive = (first, second, message) -> {
    };

    private Assertion notEqual = (first, second, message) -> {
    };

    private Assertion moreThan = (first, second, message) -> {
    };

}
