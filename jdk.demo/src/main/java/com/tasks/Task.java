package com.tasks;

public class Task {

    private boolean inProgress = false;
    private int attemptsMade = 0;
    private int attemptsLimit;

    public Task(int attemptsLimit) {
        this.attemptsLimit = attemptsLimit;
    }

    public void run() {
        System.out.println("-------- run вызван");
        if (!isInProgress()) {
            if (attemptsMade <= attemptsLimit) {
                startAttempt();
            }
        }
        System.out.println("run завершен");
    }

    private void startAttempt() {
        System.out.println("начало билда");
        attemptsMade++;
    }

    public boolean isInProgress() {
        System.out.println("проверка: уже в работе?");
        return inProgress;
    }

    public void state(boolean st) {
        inProgress = st;
    }

}
