package com;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

/**
 * 3693  3692
 * 3630  3585
 * 3563  3554
 * 3587  3533
 * 3576  3540
 * 3561  3538
 * 3560  3521
 * 3601  3536
 * 3570  3535
 * 3567  3574
 *
 * 4998
 * 4977
 * 4949
 * 4967
 * 4974
 * 4944
 * 4979
 * 4935
 * 4962
 * 4966
 *
 */
class SimpleIdentityMap {

    private static final int COUNT = 100_000_000;


    @RepeatedTest(value = 10)
    void doIt() {
        long start = System.currentTimeMillis();

        for (int i = 0; i < COUNT; i++) {
            AssertionFactory.Assertion ass = AssertionFactory.forType("не пусто4");
            ass.compare("", "", "");
        }
        System.out.println(System.currentTimeMillis() - start);
    }

    @RepeatedTest(value = 10)
    void doIt2() {
        long start = System.currentTimeMillis();

        for (int i = 0; i < COUNT; i++) {
            AssertionFactorySwitch.Assertion ass = AssertionFactorySwitch.forType("не пусто4");
            ass.compare("", "", "");
        }
        System.out.println(System.currentTimeMillis() - start);
    }

}
