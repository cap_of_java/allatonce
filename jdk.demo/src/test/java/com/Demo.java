package com;

import com.assertions.AssertFactory;
import com.assertions.Assertion;
import com.assertionswitch.StringAssertion;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

class Demo {


    @Test
    void doItAgain(List<List<String>> dataTable) {
        List<String> errors = dataTable.parallelStream()
                .map(x -> {
                    Assertion assertion = AssertFactory.forType(x.get(1));
                    return assertion.compare(x.get(0), x.get(2));
                })
                .filter(x -> !x.isEmpty())
                .collect(Collectors.toList());
        if (!errors.isEmpty()) {
            throw new RuntimeException(AssertFactory.errorMsg(errors));
        }
    }


    @Test
    void doItAgainAndAgain() {
        StringAssertion.forType("равно")
                .apply("1", "2");
    }

}
