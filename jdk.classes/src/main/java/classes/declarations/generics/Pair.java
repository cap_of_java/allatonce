package classes.declarations.generics;

public class Pair<K extends CharSequence, V> {

    K key;
    V value;
    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }
}
