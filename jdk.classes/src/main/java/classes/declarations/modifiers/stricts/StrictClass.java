package classes.declarations.modifiers.stricts;

/**
 * The effect of the strictfp modifier is to make all float or double expressions
 * within the class declaration (including within variable initializers, instance
 * initializers, static initializers, and constructors) be explicitly FP-strict (§15.4).
 * This implies that all methods declared in the class, and all nested types declared in
 * the class, are implicitly strictfp.
 */
public strictfp class StrictClass {
}
