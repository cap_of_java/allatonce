package classes.declarations.modifiers.abstracts;

/**
 * abstract means incomplete or meant to be incomplete
 * <p>
 * it's a compile time error if an attempt is made to create instance of abstract class
 * <p>
 * subclass (that is not abstract) may be instantiated
 * resulting in execution of constructor for the abstract class and field initializers for instance variables of the class
 * <p>
 * only abstract classes may have abstract methods, declared but not implemented
 * It's a compile error if not abstract class has an abstract method
 * <p>
 * class C has abstract methods if following is true:
 * 1. any of members methods of C (declared or inherited) is abstract
 * 2. any of C super classes has an abstract method
 */
public abstract class Point {

    private static Object fieldStatic = instantiateStatic();
    private Object fieldInstance = instantiateInstance();
    int x = 1, y = 1;

    public Point() {
        System.out.println("Point class constructor");
    }

    public void move(int dx, int dy){
        x += dx;
        y += dy;
        alert();
    }

    abstract void alert();

    private static Object instantiateStatic() {
        System.out.println("Point class static instance field initializer");
        return new Object();
    }

    private Object instantiateInstance() {
        System.out.println("Point class instance field initializer");
        return new Object();
    }

}

