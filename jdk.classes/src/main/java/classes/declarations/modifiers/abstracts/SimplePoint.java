package classes.declarations.modifiers.abstracts;


public class SimplePoint extends Point {

    @Override
    void alert() {
        System.out.println("alert() from " + this.getClass());
    }
}
