package classes.declarations.modifiers.finals;

/**
 * class may be defined final if it's definition is complete and no subclasses are desired or required
 *
 * 1. It's compile time error if any class tries to extend class that defined final
 * 2. It's compile time error if any class declared both final and abstract
 *
 * methods of final class will never be overridden
 */
public final class FinalClass {
}
