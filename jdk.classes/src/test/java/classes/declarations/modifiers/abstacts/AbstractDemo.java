package classes.declarations.modifiers.abstacts;

import classes.declarations.modifiers.abstracts.Point;
import classes.declarations.modifiers.abstracts.SimplePoint;
import org.junit.jupiter.api.Test;

public class AbstractDemo {

    @Test
    void doIt() {
        Point point = new SimplePoint();
        point.move(1,1);
    }

}

/**
 * results compile time error
 * it would be impossible  any subclass of class Colored to provide an implementation of the method named setColor(int color)
 * to satisfy both abstract method specifications the one in Colorable return no value
 * while the other one in class Colored requires the same method to return a value of type int
 */
interface Colorable {
    void setColor(int color);
}

//abstract class Colored implements Colorable {
//    public abstract int setColor(int color);
//}
