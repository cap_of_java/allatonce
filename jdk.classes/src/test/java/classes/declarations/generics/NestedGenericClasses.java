package classes.declarations.generics;

import org.junit.jupiter.api.Test;

public class NestedGenericClasses {

    @Test
    void doIt0() {
        Seq<String> strings = new Seq<>(
                "a",
                new Seq<String>(
                        "b",
                        new Seq<String>()
                )
        );

        Seq<Number> numbers = new Seq<>(
                1,
                new Seq<>(
                        1.5,
                        new Seq<>()
                )
        );

        Seq<String>.Zipper<Number> zipper = strings.new Zipper<Number>();

        Seq<Pair2<String, Number>> combined = zipper.zip(numbers);

        System.out.println(combined);
    }

}

class Seq<T> {
    T head;
    Seq<T> tail;

    public Seq() {
        this(null, null);
    }

    public Seq(T head, Seq<T> tail) {
        this.head = head;
        this.tail = tail;
    }

    boolean isEmpty() {
        return tail == null;
    }

    class Zipper<S> {
        Seq<Pair2<T,S>> zip(Seq<S> that) {
            if (isEmpty() || that.isEmpty()) {
                return new Seq<Pair2<T,S>>();
            } else {
                Seq<T>.Zipper<S> tailZipper =
                        tail.new Zipper<S>();
                return new Seq<Pair2<T,S>>(
                        new Pair2<T,S>(head, that.head),
                        tailZipper.zip(that.tail));
            }
        }
    }

    @Override
    public String toString() {
        return "{" +
                "head: " + head +
                ", tail: " + tail +
                '}';
    }
}

class Pair2<T, S> {
    T first;
    S second;

    public Pair2(T first, S second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return "{" +
                "first: " + first +
                ", second: " + second +
                '}';
    }
}