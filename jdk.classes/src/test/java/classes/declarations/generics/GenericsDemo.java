package classes.declarations.generics;

import org.junit.jupiter.api.Test;

public class GenericsDemo {

    @Test
    void doIt() {
        Pair<CharSequence, Integer> pair = new Pair<>("test", 1);
        Pair<CharSequence, Integer> pair2 = new Pair<>(new StringBuffer("test2"), 1);
        pair.key = "test2";
    }


    /**
     * Mutually recursive Type variable bounds
     */
    @Test
    void doIt2() {


    }

    interface ConvertibleTo<T> {
        T convert();
    }

    class ReprChange<T extends ConvertibleTo<S>, S extends ConvertibleTo<T>> {
        T t;

        void set(S s) {
            t = s.convert();
        }

        S get() {
            return t.convert();
        }
    }

    class XML implements ConvertibleTo<String> {

        private String value;

        public XML(final String value) {
            this.value = value;
        }

        @Override
        public String convert() {
            return value;
        }
    }

}
