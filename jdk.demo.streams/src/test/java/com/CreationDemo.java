package com;

import org.junit.jupiter.api.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CreationDemo {

    /**
     * неудобен потому что требует предварительно создать массив
     * но хорошо работает для args
     */
    @DisplayName("Создание потока с помощью метода stream класса Arrays")
    @Test
    void streamFromArray() {
        String[] arr = new String[]{"one", "two", "three"};
        Arrays.stream(arr).forEach(System.out::println);

        int[] intArray = new int[]{1, 2, 3};
        Arrays.stream(intArray).forEach(System.out::println);  // перегруженная версия для int[], а так же есть для double[] и long[]
    }

    @DisplayName("Создание потока с помощью статичной фабрики интерфейса Stream")
    @Test
    void streamFromStaticFactory() {
        Stream.of("one", "tow", "three").forEach(System.out::println);

        Stream.of("one").forEach(System.out::println);  // перегруженная версия, принимающая один элемент и создающая стрим из одного элемента
    }

    @DisplayName("Создание бесконечного потока с помощью метода Stream.iterate")
    @Test
    void eternalStream() {
        // принимает первичный(нулевой элемент), а потом бесконечно применяет к нему указанную функцию
        List<BigDecimal> nums = Stream.iterate(BigDecimal.ONE, n -> n.add(BigDecimal.ONE))
                .limit(10)  // ограничивает бесконечный поток
                .collect(Collectors.toList());
        System.out.println(nums);
    }

    @DisplayName("Последовательный неупорядоченный поток повторно вызывая Supplier")
    @Test
    void generateStream() {
        String res = Stream.generate(() -> "ха")
                .limit(10).collect(Collectors.joining());
        System.out.println(res);
    }

    @DisplayName("Создание потока с помощью метода stream интерфейса Collection")
    @Test
    void streamFromIterable() {
        Collection<String> list = Arrays.asList("ha", "b", "r");
        String habr = list.stream()
                .collect(Collectors.joining());
        Assertions.assertEquals("habr", habr);
    }

    /**
     * существует 3 родственных для Stream интерфейса
     * IntStream LongStream and DoubleStream
     *
     * Они имеют по два дополнительных фабричных метода для создания потоков
     */
    @Test
    void intStreamRange() {
        int[] ints = IntStream.range(1, 5).toArray();
        Assertions.assertArrayEquals(new int[]{1, 2, 3, 4}, ints);
    }

    @Test
    void intStreamRangeClosed() {
        int[] ints = IntStream.rangeClosed(1, 5).toArray();
        Assertions.assertArrayEquals(new int[]{1, 2, 3, 4, 5}, ints);
    }


    @Nested
    class Boxing {

        List<Integer> expected;

        @BeforeEach
        void init() {
            expected = new ArrayList<>();
            expected.add(1);
            expected.add(2);
            expected.add(3);
        }

        @Test
        void willNotCompile() {
//            IntStream.range(1, 3).collect(Collectors.toList());
        }

        @Test
        void methodBoxed() {
            List<Integer> generated = IntStream.range(1, 4)
                    .boxed()
                    .collect(Collectors.toList());
            Assertions.assertEquals(expected, generated);
        }

        @Test
        void methodMapToObj() {
            List<Integer> generated = IntStream.range(1, 4)
                    .mapToObj(Integer::valueOf)
                    .collect(Collectors.toList());
            Assertions.assertEquals(expected, generated);
        }

        @Test
        void methodCollectWithThreeArguments() {
            List<Integer> generated = IntStream.range(1, 4)
                    .collect(ArrayList<Integer>::new, ArrayList::add, ArrayList::addAll);
            Assertions.assertEquals(expected, generated);
        }
    }

    @Test
    void primitiveStreamToArray() {
        int[] ints = IntStream.of(1, 2, 3).toArray();  // generated Object[]
        Assertions.assertArrayEquals(new int[]{1, 2, 3}, ints);
    }


}
