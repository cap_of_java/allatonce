package com;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReductionDemo {

    /**
     * IntStream LongStream and DoubleStream
     * у них есть встроенные операции редукции
     */
    @Test
    void embeddedReduction() {
        Assertions.assertAll(
                () -> assertEquals(3, IntStream.rangeClosed(1, 3).count()),
                () -> assertEquals(OptionalInt.of(5), IntStream.rangeClosed(1, 5).max()),
                () -> assertEquals(OptionalInt.of(1), IntStream.rangeClosed(1, 5).min()),
                () -> assertEquals(6, IntStream.rangeClosed(1, 3).sum()),
                () -> assertEquals(OptionalDouble.of(3), IntStream.rangeClosed(1, 5).average())
        );
    }

    @Test
    void summaryIntStream() {
        System.out.println(IntStream.range(-5, 110).summaryStatistics());
    }

    @Test
    void reduceIntStream() {
        Assertions.assertAll(
                () -> assertEquals(OptionalInt.of(6), IntStream.range(1, 4)
                        .reduce((left, right) -> left + right)),

                () -> assertEquals(6, IntStream.range(1, 4)
                        .reduce((left, right) -> left + right).orElse(0))
        );
    }

    @Test
    void reduceIntStreamWithIdentity() {
        Assertions.assertAll(
                // тут получилось 109 потому что первое значение в стриме было использовано в качестве аккумулятора и не было умножено на 2
                () -> assertEquals(109, IntStream.rangeClosed(1, 10)
                        .reduce((acc, increment) -> acc + 2 * increment).orElse(0)),

                // во втором примере такой проблемы нет, так как задано начальное значение для аккумулятора
                () -> assertEquals(110, IntStream.rangeClosed(1, 10)
                        .reduce(0, (acc, increment) -> acc + 2 * increment))
        );
    }


    /**
     * в классы Integer Long Double добавлены функции для редукции
     * sum(int a, int b)
     */
    @Test
    void IntegerSum() {
        assertEquals(55, Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .reduce(0, Integer::sum));
    }

    @Test
    void maxUsingReduction() {
        assertEquals(10, Stream.of(3, 1, 5, 6, 3, 10)
                .reduce(Integer.MIN_VALUE, Math::max));
    }

    @Test
    void concatenationUsingReduction() {
        assertEquals("aaaAAbb", Stream.of("aaa", "AA", "bb")
                .reduce("", String::concat));
    }

    @Test
    void concatenationUsingCollector() {
        assertEquals("aaaAAbb", Stream.of("aaa", "AA", "bb")
                .collect(
                        () -> new StringBuilder(), // хранилище промежуточного результата
                        (sb, str) -> sb.append(str),  // добавление новой строки к промежуточному результату
                        (sb1, sb2) -> sb1.append(sb2))  // соединение двух промежуточных результатов
                .toString());
    }

    @Test
    void concatenationUsingJoin() {
        assertEquals("aaaAAbb", Stream.of("aaa", "AA", "bb")
                .collect(Collectors.joining()));
    }

    @Test
    void reduceForGenerics() {
        class Book {
            private Integer id;
            private String title;

            public Book(Integer id, String title) {
                this.id = id;
                this.title = title;
            }

            public Integer getId() {
                return id;
            }

            public String getTitle() {
                return title;
            }
        }

        List<Book> books = new ArrayList<>();
        // что бы пример был нагляден, надо накидать книг в коллекцию
        // но мне нужен только пример, как реализуется редукция
        Map<Integer, String> bookMap = books.stream()
                .reduce(new HashMap<>(),
                        (map, element) -> {
                            map.put(element.getId(), element.getTitle());
                            return map;
                        },
                        (map1, map2) -> {
                            map1.putAll(map2);
                            return map1;
                        });

    }
}
