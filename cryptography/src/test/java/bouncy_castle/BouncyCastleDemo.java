package bouncy_castle;

import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.junit.jupiter.api.Test;
import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class BouncyCastleDemo {

    @Test
    void doIt() throws NoSuchAlgorithmException, OperatorCreationException, IOException {
        doStuff();
    }


    private void doStuff() throws OperatorCreationException, NoSuchAlgorithmException, IOException {
        KeyPair pair = KeyPairGenerator.getInstance("RSA").generateKeyPair();


        JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder("SHA256withRSA");
        ContentSigner signer = csBuilder.build(pair.getPrivate());


        PKCS10CertificationRequestBuilder p10Builder = new JcaPKCS10CertificationRequestBuilder(
                new X500Principal("CN=Requested Test Certificate"), pair.getPublic());


        PKCS10CertificationRequest csr = p10Builder.build(signer);

        System.out.println(Base64.getEncoder().encodeToString(csr.getEncoded()));
        System.out.println("-----------------");
        System.out.println("-----------------");


    }


//    @Test
//    void doAnother() {
//        new PKCS10()
//    }

}
