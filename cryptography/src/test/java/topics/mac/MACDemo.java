package topics.mac;


import org.junit.jupiter.api.Test;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**
 * javax.crypto.Mac создает код аутентификации сообщения (Message Authentication Code) из двоичных данных
 * <p>
 * MAC - это это хэш сообщения зашиврованный секретным ключо
 */
public class MACDemo {

    public Key getSecretKey() {
        byte[] keyBytes = new byte[]{0,1,2,3,4,5,6,7,8 ,9,10,11,12,13,14,15};
        return new SecretKeySpec(keyBytes, "RawBytes");
    }

    @Test
    void initAndUsage() throws NoSuchAlgorithmException, InvalidKeyException {
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(getSecretKey());

        // для одного блока данных
        byte[] data  = "abcdefghijklmnopqrstuvxyz".getBytes(StandardCharsets.UTF_8);
        byte[] macBytes = mac.doFinal(data);


        // для нескольких блоков данных
        byte[] data1  = "abcdefghijklmnopqrstuvxyz".getBytes(StandardCharsets.UTF_8);
        byte[] data2 = "0123456789".getBytes(StandardCharsets.UTF_8);

        mac.update(data1);
        mac.update(data2);

        byte[] twoBlocks = mac.doFinal();
    }

}
