package topics.csr;


import org.junit.jupiter.api.Test;
import sun.security.pkcs10.PKCS10;
import sun.security.x509.X500Name;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Base64;

/**
 * Данный способ работает только на JDK 1.8
 * <p>
 * CSR - Certificate Signing Request
 * Это сгенерированный запрос к Certificate Authority для создания подписанного сертификата
 * <p>
 * шаги для создания CSR
 * <p>
 * 1. Создать/получить ключевую пару, для которой создается запрос
 * её можно создать с помощью {@link java.security.KeyPairGenerator}
 * или загрузить из {@link java.security.KeyStore}
 * второй способ предпочтительней
 * 2. Создать Субъект для запроса, а так же создать signature для запроса CSR
 * 3. Подписать запрос CSR приватным ключом из ключевой пары
 * 4. Получить закодированные данные CSR запроса и сохранить их, а затем отправить к CA для создания сертификата
 */
public class HowToCreateCSR {

    private static final String PASSWORD = "123";
    private static final char[] PASS_ARRAY = PASSWORD.toCharArray();
    private static final String keyStoreFile = "storage.jks";
    private KeyStore keyStore;

    public HowToCreateCSR() {
        createOrLoad();
    }

    private void createOrLoad() {
        if (new File(keyStoreFile).exists()) {
            loadDefaultKeyStore();
        } else {
            createDefaultKeyStore();
        }
    }

    private void loadDefaultKeyStore() {
        System.out.println("load default store");
        try (InputStream is = new FileInputStream(keyStoreFile)) {
            keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(is, PASS_ARRAY);
            System.out.println("load successful");
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
            throw new RuntimeException("Упс load!!!", e);
        }
    }

    private void createDefaultKeyStore() {
        System.out.println("create new default store");
        try {
            keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(null, PASS_ARRAY);
            System.out.println("creation are successful");
        } catch (IOException | KeyStoreException | CertificateException | NoSuchAlgorithmException e) {
            throw new RuntimeException("Упс create!!!", e);
        }
    }

    private void storeToDefault() {
        try (OutputStream os = new FileOutputStream(keyStoreFile)) {
            keyStore.store(os, PASS_ARRAY);
        } catch (IOException | KeyStoreException | CertificateException | NoSuchAlgorithmException e) {
            throw new RuntimeException("Упс store!!!", e);
        }
    }

    @Test
    void createCRSAndSaveKeyPair() {
        KeyPair keyPair = generateKeyPair("RSA", 2048);

        saveKeyPair(keyPair);

        byte[] csr = generateCSR("SHA256WithRSA", keyPair);
        String csrPem = CSRBase64(csr);
        System.out.println(csrPem);
    }

    @Test
    void loadKeyPair() throws UnrecoverableEntryException, NoSuchAlgorithmException, KeyStoreException {
        byte[] privateKey = getSecret("pri");
        byte[] publicKey = getSecret("pub");

        System.out.println(Arrays.toString(privateKey));
        System.out.println(Arrays.toString(publicKey));

    }

    private void saveKeyPair(KeyPair keyPair) {
        byte[] publicKey = keyPair.getPublic().getEncoded();
        byte[] privateKey = keyPair.getPrivate().getEncoded();
        try {
            System.out.println(PublicBase64(publicKey));
            saveSecret("pub", publicKey);
            System.out.println(RSAPrivateBase64(privateKey));
            saveSecret("pri", privateKey);
            storeToDefault();
        } catch (KeyStoreException e) {
            throw new RuntimeException("save key pair " + e.getMessage(), e);
        }

    }

    private void saveSecret(String alias, byte[] content) throws KeyStoreException {
        SecretKey secretKey = new SecretKeySpec(content, "AES");
        KeyStore.SecretKeyEntry secret = new KeyStore.SecretKeyEntry(secretKey);
        keyStore.setEntry(alias, secret, new KeyStore.PasswordProtection(PASS_ARRAY));
    }

    private byte[] getSecret(String alias) throws UnrecoverableEntryException, NoSuchAlgorithmException, KeyStoreException {
        KeyStore.Entry entry = keyStore.getEntry(alias, new KeyStore.PasswordProtection(PASS_ARRAY));
        if (entry instanceof KeyStore.SecretKeyEntry) {
            return ((KeyStore.SecretKeyEntry) entry).getSecretKey().getEncoded();
        } else {
            throw new RuntimeException("Get secret wrong Entry type");
        }
    }

    private KeyPair generateKeyPair(String alg, int keySize) {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(alg);
            keyPairGenerator.initialize(keySize);
            return keyPairGenerator.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Такого алгоритма не существует " + alg, e);
        }
    }

    private byte[] generateCSR(String signAlgorithm, KeyPair keyPair) {
        try {
            X500Name x500Name = new X500Name("CN=EXAMPLE.COM");

            Signature signature = Signature.getInstance(signAlgorithm);
            signature.initSign(keyPair.getPrivate());

            PKCS10 pkcs10 = new PKCS10(keyPair.getPublic());
            pkcs10.encodeAndSign(x500Name, signature);

            pkcs10.print(System.out);
            return pkcs10.getEncoded();
        } catch (NoSuchAlgorithmException | IOException | CertificateException | InvalidKeyException | SignatureException e) {
            throw new RuntimeException("Что то пошло не так при генерации CSR", e);
        }
    }

    private String CSRBase64(byte[] bytes) {
        return "-----BEGIN CERTIFICATE REQUEST-----\n" + asBase64(bytes) +
                "\n-----END CERTIFICATE REQUEST-----";
    }

    private String PublicBase64(byte[] bytes) {
        return "-----BEGIN PUBLIC KEY-----\n" + asBase64(bytes) +
                "\n-----END PUBLIC KEY-----";
    }

    private String RSAPrivateBase64(byte[] bytes) {
        return "-----BEGIN RSA PRIVATE KEY-----\n" + asBase64(bytes) +
                "\n-----END RSA PRIVATE KEY-----";
    }

    private String asBase64(byte[] bytes) {
        return Base64.getUrlEncoder().encodeToString(bytes);
    }


    @Test
    void attemptToSign() throws IOException {
        Process p = Runtime.getRuntime().exec("C:\\Program Files\\Git\\usr\\bin\\openssl genrsa 2048");
        InputStream is = p.getInputStream();
        int i = 0;
        StringBuffer sb = new StringBuffer();
        while ((i = is.read()) != -1) {
            sb.append((char) i);
        }
        String privateKeyPem = sb.toString().trim();
        System.out.println(privateKeyPem);
    }
//
//    @Test
//    void another() throws IOException {
//        String privateKey = openSSL("genrsa 2048");
//
//        String result = openSSL("rsa -pubout\n" + privateKey + "\n");
//        System.out.println(result);
//    }


    @Test
    void doIt() throws IOException {
        Process genrsa = openSSL("genrsa 2048");
        String privateKey = read(genrsa);
        System.out.println(privateKey);

        Process pubout = openSSL("rsa -pubout");
        String publicKey = write(pubout, privateKey);
        System.out.println(publicKey);

        Process req = openSSL("req -new -key\n");
        String result = write(req, privateKey);
        System.out.println(result);
    }

    private String write(Process p, String content) throws IOException {
        OutputStream os = p.getOutputStream();
        os.write(content.getBytes(StandardCharsets.UTF_8));
        os.flush();
        return read(p);
    }

    private String read(Process p) throws IOException {
        InputStream is = p.getInputStream();
        int i = 0;
        StringBuffer sb = new StringBuffer();
        while ((i = is.read()) != -1) {
            sb.append((char) i);
        }
        return sb.toString();
    }

    private Process openSSL(String cmd) throws IOException {
        String path = "\"C:\\Program Files\\Git\\usr\\bin\\openssl\"";
        String command = path + " " + cmd;
        return Runtime.getRuntime().exec(command);
    }

    private String readFrom(Process p) throws IOException {
        InputStream is = p.getInputStream();
        int i = 0;
        StringBuffer sb = new StringBuffer();
        while ((i = is.read()) != -1) {
            sb.append((char) i);
        }
        return sb.toString();
    }

//    private String readAndWriteFrom(String cmd, String write) throws IOException {
//        Process p = Runtime.getRuntime().exec("\"C:\\Program Files\\Git\\usr\\bin\\openssl\" " + cmd);
//        int i = 0;
//        StringBuffer sb = new StringBuffer();
//        while ((i = is.read()) != -1) {
//            sb.append((char) i);
//        }
//        return sb.toString();
//    }

}
