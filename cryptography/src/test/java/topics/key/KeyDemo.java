package topics.key;

import org.junit.jupiter.api.Test;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.*;

/**
 * java.security.KeyPair
 *     представляет собой пару ассиметричных ключей (открытый и закрытый)
 *     обычно полуают из хранилища ключей Java или из KeyPairGenerator
 *
 * javax.crypto.KeyGenerator
 *     используется для генерации симметричных ключей шифрования
 *
 * java.security.KeyPairGenerator
 *     используется для генерации ассиметричных ключевых пар
 *     Пара ассиметричных ключей состоит из двух ключей:
 *         первый ключ обычно используется для шифрования данных
 *         а второй для расшифровки данных, зашифрованных первым ключом
 */
public class KeyDemo {

    @Test
    void accessToPublicAndPrivateKeysFromKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();
    }

    @Test
    void symmetricKeyGenerator() throws NoSuchAlgorithmException {
        // получение генератора
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");

        // инициализация генератора, принимает длинну ключа и SecureRandom, который испоьзуется во время генерации ключа
        SecureRandom secureRandom = new SecureRandom();
        int keyBitSize = 256;
        keyGenerator.init(keyBitSize, secureRandom);

        // генерация ключа
        SecretKey secretKey = keyGenerator.generateKey();
    }


    /**
     * Наиболее известным типом ассиметричных ключей является тип пары ключей вида: открытый ключ + закрытый ключ
     * Закрытый ключ используется для шифрования данных, а открытый для расшифровки данных
     * !!! На самом деле вы можете так же зашифровать данные с помощью открытого ключа и расшифровать с помощью закрытого
     *
     * Закрытый ключ обычно хранится в секрете
     * А открытый ключ может быть известен всем
     *
     * Т.о. если кто то шифрует некоторые данных закрытым ключом, то тот кто владеет открытым ключом может расшифровать их
     */
    @Test
    void privateAndPublicKeys() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);

        KeyPair keyPair = keyPairGenerator.generateKeyPair();
    }


}
