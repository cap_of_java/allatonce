package topics.cipher;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import javax.crypto.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Cipher - Шифр
 */
public class CipherDemo {

    @Nested
    class CipherCreation {
        /**
         * Что бы получить instance нужно вызывать метод getInstance с именем алгоритма шифрования, который нужно получить
         */
        @Test
        void getInstance() throws NoSuchPaddingException, NoSuchAlgorithmException {
            Cipher aes = Cipher.getInstance("AES");
            assertEquals("AES", aes.getAlgorithm());
        }

        @Test
        void attemptToGetNonExistentAlgorithm() {
            assertThrows(NoSuchAlgorithmException.class, () -> Cipher.getInstance("ora ora ora"));
        }

        /**
         * Алгоритмы шифрования могут работать в различных режимах
         * Что бы создать Cipher c другим режимом работы, можно добавить его режим работы в метод getInstance
         * <p>
         * CBC - Cipher Block Chaining
         * Поскольку режим сцепления блоков шифрования также требует «схемы дополнения»,
         * схема дополнения (PKCS5Padding) добавляется в конец строки имени алгоритма шифрования
         */
        @Test
        void cipherAlgorithmMods() throws NoSuchPaddingException, NoSuchAlgorithmException {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            assertEquals("AES/CBC/PKCS5Padding", cipher.getAlgorithm());
        }
    }


    /**
     * Для инициализации Cipher нужно указать
     * режим работы
     * Cipher.ENCRYPT_MODE
     * Cipher.DECRYPT_MODE
     * симметричный ключ
     */
    @Nested
    class Initialization {

        Key aesKey = getAESKey();

        @Test
        void encryptMode() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
        }

        @Test
        void decryptMode() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, aesKey);
        }

    }

    private Key getAESKey() {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            SecureRandom secureRandom = new SecureRandom();
            int keyBitSize = 256;
            keyGenerator.init(keyBitSize, secureRandom);
            return keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    @Nested
    class EncryptionDecryption {

        Key key = getAESKey();
        String plainTextStr = "abcdefghijklmnopqrstuvwxyz";

        /**
         * Небольшой блок текста
         * просто вызывайте update или doFinal
         */
        @Test
        void encryptDecrypt() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainText  = plainTextStr.getBytes(StandardCharsets.UTF_8);
            byte[] cipherText = cipher.doFinal(plainText);

            Cipher decryptCipher = Cipher.getInstance("AES");
            decryptCipher.init(Cipher.DECRYPT_MODE, key);
            byte[] decryptedText = decryptCipher.doFinal(cipherText);

            assertEquals(plainTextStr, new String(decryptedText, StandardCharsets.UTF_8));
        }

        /**
         * Большой файл разбитый на множество блоков
         *
         * Причина, по которой вызов doFinal() необходим для последнего блока данных, заключается в том,
         * что некоторые алгоритмы шифрования должны дополнять данные, чтобы соответствовать определенному размеру блока шифра (например, 8-байтовой границе)
         */
        @Test
        void severalTextBlocks() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
            Charset charset = StandardCharsets.UTF_8;
            byte[] data1 = "abcdefghijklmnopqrstuvwxyz".getBytes(charset);
            byte[] data2 = "zyxwvutsrqponmlkjihgfedcba".getBytes(charset);
            byte[] data3 = "01234567890123456789012345".getBytes(charset);


            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] cipherText1 = cipher.update(data1);
            byte[] cipherText2 = cipher.update(data2);
            byte[] cipherText3 = cipher.doFinal(data3);
        }


        @Test
        void partialByteArrayEncryptionDecryption() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] data = "abcdefghijklmnopqrstuvwxyz5555555555555555555555".getBytes(StandardCharsets.UTF_8);
            int offset = 10;
            int length = 24;
            byte[] cipherText = cipher.doFinal(data, offset, length);
        }

        /**
         * Все приведенные в этой главе примеры шифрования и дешифрования возвращают зашифрованные или дешифрованные данные в новом байтовом массиве.
         * Однако также возможно зашифровать или расшифровать данные в существующий байтовый массив.
         * Это может быть полезно для уменьшения количества созданных байтовых массивов.
         */
        @Test
        void workWithExistingByteArray() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, ShortBufferException, IllegalBlockSizeException {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] data = "abcdefghijklmnopqrstuvwxyz5555555555555555555555".getBytes(StandardCharsets.UTF_8);
            int offset = 10;
            int length = 24;
            byte[] dest = new byte[1024];  // existing byte array
            // в этом случае расшифрованные байты будут размежены в начале массива dest
            cipher.doFinal(data, offset, length, dest);

//            int offset = 10;
//            int length = 24;
            byte[] dest2 = new byte[1024];
            int destOffset = 12;
            // в этом случае байты будут размещены в массиве dest2 с отступом от начала 12
            cipher.doFinal(data, offset, length, dest2, destOffset);
        }

    }


    /**
     *Инициализация экземпляра Cipher — дорогостоящая операция
     * и хорошей идеей будет повторное использование экземпляров Cipher.
     * К счастью, класс Cipher был разработан с учетом возможности повторного использования.
     */
    @Test
    void reusingCipher() throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        Charset charset = StandardCharsets.UTF_8;
        Cipher cipher = Cipher.getInstance("AES");

        Key key = getAESKey();
        cipher.init(Cipher.ENCRYPT_MODE, key);

        byte[] data1 = "abcdefghijklmnopqrstuvwxyz".getBytes(charset);
        byte[] data2 = "zyxwvutsrqponmlkjihgfedcba".getBytes(charset);

        byte[] cipherText1 = cipher.update(data1);
        byte[] cipherText2 = cipher.doFinal(data2); // сначала шифруем один блок данных

        byte[] data3 = "01234567890123456789012345".getBytes(charset);
        byte[] cipherText3 = cipher.doFinal(data3);  // затем шифруем другой блок данных

        cipher.init(Cipher.DECRYPT_MODE, key);  // изменяем режим работы на дешифровку
        byte[] decrypted = cipher.doFinal(cipherText3);  // и теперь уже делаем расшифровку используя тот же самый объект шифра
        assertEquals("01234567890123456789012345", new String(decrypted, charset));
    }

}
