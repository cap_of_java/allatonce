package topics.messageDigest;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Класс Java MessageDigest представляет криптографическую хеш-функцию, которая может вычислять дайджест сообщения из двоичных данных.
 */
public class MessageDigestDemo {

    /**
     * MD2  MD5  SHA-1  SHA-256  SHA-384  SHA-512
     *
     * Не все эти алгоритмы одинаково безопасны. На момент написания статьи рекомендуется использовать SHA-256 или выше
     */
    @Test
    void getInstance() throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
    }

    @Test
    void oneBlockOfData() throws NoSuchAlgorithmException {
        byte[] data1 = "0123456789".getBytes(StandardCharsets.UTF_8);

        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] digest = messageDigest.digest(data1);
    }

    @Test
    void multipleBlocksOfData() throws NoSuchAlgorithmException {
        byte[] data1 = "0123456789".getBytes(StandardCharsets.UTF_8);
        byte[] data2 = "abcdefghijklmnopqrstuvxyz".getBytes(StandardCharsets.UTF_8);

        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(data1);
        messageDigest.update(data2);

        byte[] digest = messageDigest.digest();
    }

}
