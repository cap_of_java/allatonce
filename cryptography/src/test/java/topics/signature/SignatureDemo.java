package topics.signature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Arrays;

/**
 * java.security.Signature создает цифровую подпись двоичных данных
 * Цифровая подпись - это хэш код сообщения зашифрованный закрытым ключом от пары закрытый/открытый ключ
 * Любой кто владет открытым ключом может проверить цифровую подпись
 */
public class SignatureDemo {

    @Test
    void getInstance() throws NoSuchAlgorithmException {
        Signature signature = Signature.getInstance("SHA256WithDSA");
    }


    /**
     * инициализируется закрытым ключом пары секретный / открытый ключ и экземпляром SecureRandom
     */
    @Test
    void initialization() throws NoSuchAlgorithmException, InvalidKeyException {
        SecureRandom secureRandom = new SecureRandom();
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        Signature signature = Signature.getInstance("SHA256WithDSA");
        signature.initSign(keyPair.getPrivate(), secureRandom);
    }

    /**
     * Создание цифровой подписи для двоичных данных
     */
    @Test
    void createDigitalSign() throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        SecureRandom secureRandom = new SecureRandom();
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        Signature signature = Signature.getInstance("SHA256WithDSA");
        signature.initSign(keyPair.getPrivate(), secureRandom);
        byte[] data = "abcdefghijklmnopqrstuvxyz".getBytes(StandardCharsets.UTF_8);
        signature.update(data);

        byte[] digitalSignature = signature.sign();
    }

    @Test
    void digitalSignVerification() throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        SecureRandom secureRandom = new SecureRandom();
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();


        Signature signature = Signature.getInstance("SHA256WithDSA");
        signature.initSign(privateKey, secureRandom);

        byte[] data = "abcdefghijklmnopqrstuvxyz".getBytes(StandardCharsets.UTF_8);
        signature.update(data);
        byte[] digitalSignature = signature.sign();
        System.out.println(Arrays.toString(digitalSignature));

        // verification
        Signature signature2 = Signature.getInstance("SHA256WithDSA");
        // инициализируется в режиме проверки
        signature2.initVerify(publicKey);
        signature2.update(data);

        boolean verified = signature2.verify(digitalSignature);
        Assertions.assertTrue(verified);
    }

}
