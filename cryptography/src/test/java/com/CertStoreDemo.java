package com;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreParameters;
import java.security.cert.LDAPCertStoreParameters;

public class CertStoreDemo {


    @Disabled("Не понятно как с этим работать")
    @Test
    void certStore() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
        String type = CertStore.getDefaultType();

        CertStoreParameters certStoreParameters = new LDAPCertStoreParameters();


        CertStore store = CertStore.getInstance(type, certStoreParameters);

        System.out.println(store);
    }

}
