package com;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class HashDemo {

    /**
     * Что бы вычислить digest сообщения вы вызываете метод update() или digest()
     * Метод update() может вызываться несколько раз, а дайджест сообщения обновляется внутри объекта
     * Когда вы передали все данные, которые хотите включить в дайджест сообщения, вы вызываете digest()
     * и извлекаете итоговые данные дайджеста сообщения
     */
    @Test
    void messageDigestWithUpdate() throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] data1 = "0123456789".getBytes(StandardCharsets.UTF_8);
        byte[] data2 = "abcd".getBytes(StandardCharsets.UTF_8);

        md.update(data1);
        md.update(data2);
        byte[] digest = md.digest();
        System.out.println(toHex(digest));
    }

    @Test
    void messageDigestDemo() throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] data1 = "0123456789".getBytes(StandardCharsets.UTF_8);
        byte[] digest = md.digest(data1);
    }

    private String toHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

}
