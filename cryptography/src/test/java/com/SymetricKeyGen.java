package com;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class SymetricKeyGen {

    // AES - Advanced Encryption Standard
    private static final String ALGORITHM = "AES";

    @Test
    void encryptionAndDecryption() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        String key = "lv39eptlvuhaqqsr";
        String message = "Amaranth";
        String encoded = encrypt(message, key);
        String decoded = decrypt(encoded, key);

        Assertions.assertEquals(message, decoded);
    }


    private String encrypt(String data, String keyValue) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Key key = new SecretKeySpec(keyValue.getBytes(StandardCharsets.UTF_8), ALGORITHM);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = cipher.doFinal(data.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(encVal);
    }

    private String decrypt(String data, String keyValue) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Key key = new SecretKeySpec(keyValue.getBytes(StandardCharsets.UTF_8), ALGORITHM);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] rawBytes = Base64.getDecoder().decode(data.getBytes(StandardCharsets.UTF_8));
        byte[] decrypted = cipher.doFinal(rawBytes);
        return new String(decrypted, StandardCharsets.UTF_8);
    }


}
