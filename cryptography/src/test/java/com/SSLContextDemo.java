package com;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import javax.net.ssl.SSLContext;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SSLContextDemo {

    @ParameterizedTest(name = "Поддержка протокола {0}")
    @ValueSource(strings = {"TLS", "TLSv1", "TLSv1.1", "TLSv1.2"})
    void TLSandSSLInJava(String protocol) throws NoSuchAlgorithmException {
        SSLContext sc = SSLContext.getInstance(protocol);
        assertEquals(protocol, sc.getProtocol());
    }



}
