package com;

import com.configure.SluiceConfigurator;
import com.configure.model.Sluice;
import com.configure.model.Tunings;
import com.util.Jaxb;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.List;

class TestConfigurationDemo {

    @Test
    void dotI() throws JAXBException {
        String filePath = "src\\main\\resources\\adapter_tunings.xml";

        Tunings tunings = Jaxb.unmarshal(Tunings.class, new File(filePath));

        List<Sluice> sluices = tunings.getSluices("1855", "DUMMY_SMEV3");
        SluiceConfigurator configurator = new SluiceConfigurator();
        configurator.configureSluices(sluices);
        System.out.println("done");
    }

}
