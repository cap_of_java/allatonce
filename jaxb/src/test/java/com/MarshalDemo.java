package com;

import com.demo.v2.Employee;
import com.mapper.Out;
import com.configure.model.Case;
import com.configure.model.Sluice;
import com.configure.model.TestConfiguration;
import com.configure.model.Tunings;
import com.util.Jaxb;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.util.*;

class MarshalDemo {

    @Test
    void marshaling() throws JAXBException {
        Sluice gibddord7 = new Sluice("GIBDDORD7");
        gibddord7.addProperty("dummy", "yes", "переключает адаптер на использование заглушек в TEST_SLUICE вместо реальной отправки запроса к поставщику");
        gibddord7.addProperty("PROM#GIBDDORD", "1", "при включении этой проперти адаптер использует SMEV3 для платежей по документу");

        Sluice gibddord6 = new Sluice("GIBDDORD6");
        gibddord6.addProperty("TEST#ORD", "0");

        List<Sluice> sluices = new ArrayList<>();
        sluices.add(gibddord6);
        sluices.add(gibddord7);

        TestConfiguration configuration = new TestConfiguration("DUMMY_SMEV3", "ЗАПУСК НА ЗАГЛУШКЕ ГИБДД ВУ");
        configuration.setSluices(sluices);

        Case aCase = new Case("1855");
        aCase.setTestConfigurations(Collections.singletonList(configuration));

        Tunings tunings = new Tunings();
        tunings.setCases(Collections.singletonList(aCase));

        String tun = Jaxb.marshalToString(tunings);
        System.out.println(tun);
    }

    @Test
    void unmarshal() throws JAXBException {
        String filePath = "D:\\Java\\study_books\\allatonce\\jaxb\\src\\main\\resources\\adapter_tunings.xml";

        Tunings tunings = Jaxb.unmarshal(Tunings.class, new File(filePath));

        System.out.println("test");
    }

    @Test
    void mappingTest() throws JAXBException {
        Out out = new Out();

        Map<String, String> map = new HashMap<>();
        map.put("val", "ke");
        map.put("hal", "jor");

        out.setMap(map);

        String tun = Jaxb.marshalToString(out);
        System.out.println(tun);

    }


    @Test
    void withSchema() throws JAXBException, SAXException {
        JAXBContext context = JAXBContext.newInstance(Employee.class);

        Unmarshaller unmarshaller = context.createUnmarshaller();

        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File("D:\\Java\\study_books\\allatonce\\jaxb\\src\\main\\resources\\employee.xsd"));

        unmarshaller.setSchema(schema);

        Employee employee = (Employee) unmarshaller.unmarshal(new File("D:\\Java\\study_books\\allatonce\\jaxb\\src\\main\\resources\\employee.xml"));

        System.out.println(employee);
    }

}
