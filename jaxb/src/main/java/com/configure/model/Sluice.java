package com.configure.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "sluice")
public class Sluice {

    @XmlAttribute(name = "code")
    private String code;

    @XmlElementWrapper(name = "properties")
    @XmlElement(name = "property")
    private List<Property> properties = new ArrayList<>();

    public Sluice() {
        // jaxb необходим конструктор без аргументов
    }

    public Sluice(String code) {
        this.code = code;
    }

    public void addProperty(Property property) {
        properties.add(property);
    }

    public void addProperty(String key, String value) {
        properties.add(new Property(key, value));
    }

    public void addProperty(String key, String value, String desc) {
        properties.add(new Property(key, value, desc));
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }

}
