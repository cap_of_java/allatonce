package com.configure.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "configuration")
public class TestConfiguration {

    @XmlAttribute(name = "id")
    private String id;

    @XmlElement(name = "desc")
    private String description;

    @XmlElementWrapper(name = "sluices")
    @XmlElement(name = "sluice")
    private List<Sluice> sluices = new ArrayList<>();

    public TestConfiguration() {
        // jaxb необходим конструктор без аргументов
    }

    public TestConfiguration(String id) {
        this.id = id;
    }

    public TestConfiguration(String id, String description) {
        this(id);
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Sluice> getSluices() {
        return sluices;
    }

    public void setSluices(List<Sluice> sluices) {
        this.sluices = sluices;
    }
}
