package com.configure.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "tunings")
public class Tunings {

    @XmlElementWrapper(name = "cases")
    @XmlElement(name = "case")
    private List<Case> cases = new ArrayList<>();

    public List<Sluice> getSluices(String testId, String configId) {
        return findTestById(testId)
                .findConfigById(configId)
                .getSluices();
    }

    Case findTestById(String id) {
        if (id != null && !"".equals(id)) {
            for (Case testCase : cases) {
                if (testCase.getId().equalsIgnoreCase(id)) {
                    return testCase;
                }
            }
        }
        throw new IllegalArgumentException("не верные данные введены для поиска теста findTestById");
    }

    public List<Case> getCases() {
        return cases;
    }

    public void setCases(List<Case> cases) {
        this.cases = cases;
    }
}
