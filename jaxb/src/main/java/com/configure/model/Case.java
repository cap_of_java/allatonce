package com.configure.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "case")
public class Case {

    @XmlAttribute(name = "id")
    private String id;

    @XmlElementWrapper(name = "configurations")
    @XmlElement(name = "configuration")
    private List<TestConfiguration> testConfigurations = new ArrayList<>();

    public Case() {
        // jaxb необходим конструктор без аргументов
    }

    public Case(String id) {
        this.id = id;
    }

    TestConfiguration findConfigById(String id) {
        if (id != null && !"".equals(id.trim())) {
            for (TestConfiguration configuration : testConfigurations) {
                if (configuration.getId().equals(id)) {
                    return configuration;
                }
            }
        }
        throw new IllegalArgumentException("ошибка при поиске конфигурации с id " + id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<TestConfiguration> getTestConfigurations() {
        return testConfigurations;
    }

    public void setTestConfigurations(List<TestConfiguration> testConfigurations) {
        this.testConfigurations = testConfigurations;
    }
}
