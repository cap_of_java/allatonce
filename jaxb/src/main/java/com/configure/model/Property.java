package com.configure.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "property")
public class Property {

    @XmlElement(name = "key")
    private String key;

    @XmlElement(name = "value")
    private String value;

    @XmlElement(name = "desc")
    private String description;

    public Property() {
        // jaxb необходим конструктор без аргументов
    }

    public Property(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public Property(String key, String value, String description) {
        this(key, value);
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
