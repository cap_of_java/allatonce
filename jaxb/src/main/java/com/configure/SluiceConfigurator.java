package com.configure;

import com.configure.model.Property;
import com.configure.model.Sluice;

import java.util.List;

public class SluiceConfigurator {

    public void configureSluices(List<Sluice> sluices) {
        if (sluices != null && !sluices.isEmpty()) {
            for (Sluice sluice : sluices) {
                if (sluice != null) {
                    String sluiceCode = sluice.getCode();
                    if (sluiceCode != null && !"".equals(sluiceCode.trim())) {
                        if (sluice.getProperties() != null) {
                            for (Property prop : sluice.getProperties()) {
                                if (prop != null) {
                                    setUpSluiceParameter(sluiceCode, prop);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void setUpSluiceParameter(String code, Property property) {
        if (property.getKey() == null || "".equals(property.getKey().trim())) {
            System.out.println("пустая проперти. Её настройка не была выполнена");
            return;
        }
        System.out.println("Шлюз " + code + "Изменение настройки " + property.getKey() + " передано значение " + property.getValue());
    }

}
