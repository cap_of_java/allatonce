package aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * Используйте аспекты только как декораторы
 * Не пишите в них бизнес логику
 *
 * Не надо пытаться заменить логику методов аспектами
 * аспектами её можно дополнить, но не заменять
 *
 * Потому что это просто приведет к тому что код будет тяжело понять и тяжело дебажить
 */
@Aspect
@Component
public class HelloServiceAspect {

//    @Before("execution(* services.HelloService.hello(..))")
//    public void before() {
//        System.out.println("before aspect");
//    }
//
//    @After("execution(* services.HelloService.hello(..))")
//    public void after() {
//        System.out.println("after aspect");
//    }
//
//    /**
//     * executed only after return statement
//     * for example: if method throw exception than after returning will not be executed
//     */
//    @AfterReturning("execution(* services.HelloService.hello(..))")
//    public void afterReturning() {
//        System.out.println("after returning aspect");
//    }
//
//    /**
//     * works only if method throw exception
//     */
//    @AfterThrowing("execution(* services.HelloService.hello(..))")
//    public void afterThrowing() {
//        System.out.println("after throwing aspect");
//    }

    @Around("execution(* services.HelloService.hello(..))")
    public Object around(ProceedingJoinPoint joinPoint) {
        System.out.println("method inside => print something else");
        Object result = null;

        try {
            Object obj = joinPoint.proceed();
            joinPoint.proceed(new Object[]{obj});

            System.out.println(Arrays.toString(joinPoint.getArgs()));
            System.out.println(joinPoint.getArgs().length);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return "return something else";
    }

}
