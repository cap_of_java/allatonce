package core;

import config.ProjectConfig;
import services.HelloService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(ProjectConfig.class);

        HelloService hs = context.getBean(HelloService.class);
        String res = hs.hello("Mark");
        System.out.println("method return => " + res);

    }

}
