package services;

import org.springframework.stereotype.Service;

@Service
public class HelloService {

    public String hello(String name) {
        String msg = "Hello, " + name + "!";
        System.out.println(msg);
        return msg;
    }

}
