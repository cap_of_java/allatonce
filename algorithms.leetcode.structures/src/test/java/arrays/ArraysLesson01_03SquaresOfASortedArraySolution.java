package arrays;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ArraysLesson01_03SquaresOfASortedArraySolution {

    @Test
    void doIt01() {
        int[] input = new int[]{-4,-1,0,3,10};
        int[] result = sortedSquares(input);
        assertThat(result)
                .isEqualTo(new int[]{0,1,9,16,100});
    }

    @Test
    void doIt02() {
        int[] input = new int[]{-7,-3,2,3,11};
        int[] result = sortedSquares(input);
        assertThat(result)
                .isEqualTo(new int[]{4,9,9,49,121});
    }

    public int[] sortedSquares(int[] nums) {
        if (nums.length == 0) {
            return new int[0];
        }
        if (nums.length == 1) {
            return new int[]{nums[0] * nums[0]};
        }
        int insertPos = nums.length - 1;
        int[] result = new int[nums.length];
        int idxL = 0;
        int idxR = nums.length - 1;

        while (insertPos > -1) {
            // TODO наверно можно подкэшировать результаты умножения, что бы не выполнять умножение обоих чисел на каждой итерации
            // TODO или это бред и я точно так же потрачу ресурсы на if else
            int left = nums[idxL] * nums[idxL];
            int right = nums[idxR] * nums[idxR];
            int value;
            if (left > right) {
                value = left;
                idxL++;
            } else {
                value = right;
                idxR--;
            }
            result[insertPos] = value;
            insertPos--;
        }
        return result;
    }

}
