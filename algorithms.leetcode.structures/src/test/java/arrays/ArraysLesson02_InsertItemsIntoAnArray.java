package arrays;

import org.junit.jupiter.api.Test;

public class ArraysLesson02_InsertItemsIntoAnArray {


    /**
     * Вставка с использованием переменной, которая отслеживает последний добавленный элемент
     */
    @Test
    void doIt01() {
        int[] intArray = new int[6];
        int length = 0;

        for (int i = 0; i < 3; i++) {
            intArray[length] = i;
            length++;
        }
        printArray(intArray);
    }

    @Test
    void doIt02() {
        int[] intArray = new int[6];
        int length = 0;

        for (int i = 0; i < 3; i++) {
            intArray[length] = i;
            length++;
        }
        intArray[length] = 10;  // this one will override value
        printArray(intArray);
    }

    /**
     * inserting at the start of an array
     */
    @Test
    void doIt03() {
        int[] intArray = new int[6];
        int length = 0;

        for (int i = 0; i < 3; i++) {
            intArray[length] = i;
            length++;
        }
        // вставка происходит обходом с конца и смещением каждого элемента в право
        // таким обрахом образуется пространство в начале массива
        for (int i = length - 1; i >= 0; i--) {
            intArray[i + 1] = intArray[i];
        }
        intArray[0] = 20;
        printArray(intArray);
    }


    // вставка по индексу (в любую позицию массива)
    @Test
    void doIt04() {
        int[] intArray = new int[6];
        int length = 0;

        for (int i = 0; i < 3; i++) {
            intArray[length] = i;
            length++;
        }

        //
        int idx = 2, value = 30;

        for (int i = length - 1; i >= idx; i--) {
            intArray[i + 1] = intArray[i];
        }
        intArray[idx] = value;
        printArray(intArray);

    }


    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("Index " + i + " contains " + array[i]);
        }
    }

}
