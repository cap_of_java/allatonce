package arrays;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ArraysLesson01_01MaxConsecutiveOnes {

    @Test
    void doIt() {
        int[] input = new int[]{1,1,0,1,1,1};
        int result = findMaxConsecutiveOnes(input);
        assertThat(result).isEqualTo(3);
    }

    @Test
    void doIt2() {
        int[] input = new int[]{1,0,1,1,0,1};
        int result = findMaxConsecutiveOnes(input);
        assertThat(result).isEqualTo(2);
    }

    @Test
    void doIt3() {
        int[] input = new int[]{0};
        int result = findMaxConsecutiveOnes(input);
        assertThat(result).isEqualTo(0);
    }

    @Test
    void doIt4() {
        int[] input = new int[]{1};
        int result = findMaxConsecutiveOnes(input);
        assertThat(result).isEqualTo(1);
    }

    public int findMaxConsecutiveOnes(int[] nums) {
        int max = 0;
        int current = 0;
        for (int num : nums) {
            if (num == 1) {
                current++;
                if (current > max) {
                    max = current;
                }
            } else {
                current = 0;
            }
        }
        return max;
    }

}
