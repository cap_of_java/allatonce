package arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ArraysLesson01_Introduction {

    @Test
    void doIt01() {
        DVD[] collection = new DVD[15];

        // TODO does java reserves memory to hold 1000000 DVD (in this case)?
        // as computers usually do
        DVD[] collection2 = new DVD[1000000];
    }

    @Test
    void creation() {
        DVD avengersDVD = new DVD("The Avengers", 2012, "Joss Whedon");
        DVD incrediblesDVD = new DVD("The Incredibles", 2004, "Brad Bird");
        DVD findingDoryDVD = new DVD("Finding Dory", 2016, "Andrew Stanton");
        DVD lionKingDVD = new DVD("The Lion King", 2019, "Jon Favreau");

        DVD[] collection = new DVD[15];
        collection[7] = avengersDVD;
        collection[3] = incrediblesDVD;
        collection[9] = findingDoryDVD;
        collection[2] = lionKingDVD;


        DVD starWarsDVD = new DVD("Star Wars", 1977, "George Lucas");
        collection[3] = starWarsDVD;  // 3rd element is now overwritten

        // reading
        System.out.println(collection[7]);

        // TODO will print null because we haven't yet put anything there it could be different for over languages
        // for example in C language, it could be any random data
        // but java always init empty array of Objects slots with null
        System.out.println(collection[10]);  // will print null
        System.out.println(collection[3]);
    }

    @Test
    void arrayPrimitive() {
        int[] ints = new int[1];
        float[] floats = new float[1];
        boolean[] booleans = new boolean[1];

        Assertions.assertAll(
                () -> assertThat(ints).isEqualTo(new int[]{0}),
                () -> assertThat(floats).isEqualTo(new float[]{0.0f}),
                () -> assertThat(booleans).isEqualTo(new boolean[]{false})
        );
    }

    @Test
    void writingWithLoop() {
        int[] squareNumbers = new int[10];

        for (int i = 0; i < 10; i++) {
            int square = (i + 1) * (i + 1);
            squareNumbers[i] = square;
        }

        // reading
        for (int i = 0; i < 10; i++) {
            System.out.println(squareNumbers[i]);
        }

        for (int square : squareNumbers) {
            System.out.println(square);
        }
    }

    @Test
    void capacityVsLength() {
        DVD[] array = new DVD[6];

        // array can't hold more. 6 is an array capacity
//        array[6] = null;
//        array[10] = null;


        // also wouldn't work
//        array[-1] = null;

        int capacity = array.length;
        System.out.println("The Array has a capacity of " + capacity);
    }

    @Test
    void capacityVsLength02() {
        int[] array = new int[6];

        int length = 0;
        for (int i = 0; i < 3; i++) {
            array[i] = i * i;
            length++;
        }

        System.out.println("The Array has a capacity of " + array.length);
        System.out.println("The Array has a length of " + length);
    }

}
