package arrays;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ArraysLesson01_02FindNumbersWithEvenNumberOfDigits {

    @Test
    void doIt01() {
        int[] input = new int[]{12, 345, 2, 6, 7896};
        int result = findNumbers(input);
        assertThat(result).isEqualTo(2);
    }

    @Test
    void doIt02() {
        int[] input = new int[]{555,901,482,1771};
        int result = findNumbers(input);
        assertThat(result).isEqualTo(1);
    }

    public int findNumbers(int[] nums) {
        int counter = 0;
        for (int number : nums) {
            int digits = numberOfDigits(number);
            if (digits % 2 == 0) {
                counter++;
            }
        }
        return counter;
    }


    /**
     * Integer.toString(number).length
     * быстрее, но ограничимся работой с числами ради задачки
     */
    public int numberOfDigits(int number) {
        int current = number;
        int count = 0;
        do {
            count++;
            current = current / 10;
        } while (current != 0);
        return count;
    }

    @Test
    void doIt3() {
    }
}
