package arrays;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ArraysLesson02_01DuplicateZeros {

    /**
     * TODO можно ли сделать это быстрее не создавая при этом дополнительный массив?
     */
    public void duplicateZeros(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                for (int j = arr.length - 2; j >= i; j--) {
                    arr[j + 1] = arr[j];
                }
                i++;
            }
        }
    }


    @Test
    void doIt01() {
        int[] input = new int[]{1,0,2,3,0,4,5,0};
        duplicateZeros(input);
        assertThat(input).isEqualTo(new int[]{1,0,0,2,3,0,0,4});
    }

    @Test
    void doIt02() {
        int[] input = new int[]{1,2,3};
        duplicateZeros(input);
        assertThat(input).isEqualTo(new int[]{1,2,3});
    }


}
