package core;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class SearchRobots {

    @Test
    void doIt() throws InterruptedException {

        UserStatistics stat = new UserStatistics(60 * 1000, 5);

        Thread.sleep(100);
        stat.event(1);
        Thread.sleep(100);
        stat.event(1);
        Thread.sleep(100);
        stat.event(1);
        Thread.sleep(100);
        stat.event(1);
        Thread.sleep(100);
        stat.event(1);

        System.out.println(stat.getMap());
    }


    @Test
    void doIt3() {
        TreeSet<Integer> stamps = new TreeSet<>();
        stamps.add(1);
        stamps.add(2);
        stamps.add(3);
        stamps.add(4);
        stamps.add(5);

        System.out.println(stamps.tailSet(3).size());
    }

}


class UserStatistics {

    private final int period;  // millis
    private final int threshold;
    private final Map<Long, Long> map = new HashMap<>();

    public UserStatistics(int period, int threshold) {
        this.period = period;
        this.threshold = threshold;
    }

    public void event(long userId) {
        long timeStamp = currentTimeMillis();
        if (map.containsKey(userId)) {
            long value = map.get(userId);
            value++;
            map.put(userId, value);

        } else {
            map.put(userId, 1L);
        }
    }

    public int countRobots() { // O(1)
        long current = currentTimeMillis();
        long start = current - period;
        // все что больше этого момента времени



        return 0;
    }

    private long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    public Map<Long, Long> getMap() {
        return map;
    }

}